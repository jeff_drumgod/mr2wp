<?php
/**
 * A classe MR2WP_Role facilita a manipulação de capabilities.
 *
 * @author Marcel Rodrigo
 * @version 1.0
 * @date 2013-10-24
 * @category Wordpress
 * @link http://mr2wp.marcelrodrigo.com.br
 * @package MR2WP
 *
 */
class MR2WP_CapabilityType {

	private $_type;
	private $_capabilities = array();

	/**
	* Cria uma instância de MR2WP_CapabilityType
	* @param $capability_type - Nome base da capability para criação do 'type'
	*/	
	public function __construct($capability_type){
		$this->_type =  $capability_type;

		$this->_capabilities["publish_posts"] 		= "publish_{$this->_type}s";
		$this->_capabilities["edit_posts"] 			= "edit_{$this->_type}s";
		$this->_capabilities["edit_others_posts"] 	= "edit_others_{$this->_type}s";
		$this->_capabilities["delete_posts"] 		= "delete_{$this->_type}s";
		$this->_capabilities["delete_others_posts"] = "delete_others_{$this->_type}s";
		$this->_capabilities["read_private_posts"] 	= "read_private_{$this->_type}s";
		$this->_capabilities["edit_post"] 			= "edit_{$this->_type}";
		$this->_capabilities["delete_post"] 		= "delete_{$this->_type}";
		$this->_capabilities["read_post"] 			= "read_{$this->_type}";

	}

	/**
	* Retorna as capacidades do type.
	* @return array - Array com as capabilities
	*/
	public function getCapabilities(){
		return array_values($this->_capabilities);
	}
}