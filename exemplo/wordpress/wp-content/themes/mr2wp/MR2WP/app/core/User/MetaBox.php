<?php
/**
 * A classe MR2WP_User_MetaBox facilita a manipulação de meta box no perfil dos usuários
 *
 * @author Marcel Rodrigo
 * @version 1.0
 * @date 2013-10-26
 * @category Wordpress
 * @link http://mr2wp.marcelrodrigo.com.br
 * @package MR2WP
 *
 */
class MR2WP_User_MetaBox {

	private $_id;
	private $_title;
	private $_role;
	private $_metas = array();
	private $_template;

	public function __construct($id, $title){
		$this->_id = $id;
		$this->_title = $title;
		$this->_template = sprintf("%s/app/core/Templates/User/MetaBox.php", MR2WP_PATH);
	}

	public function addMeta(MR2WP_Meta $meta){
		$this->_metas[] = $meta;
		return $this;
	}

	public function setRole(MR2WP_User_Role $role){
		$this->_role = $role;
		return $this;
	}

	public function save(){
		add_action( 'show_user_profile', array(&$this, "__cb_umbx_display") );
		add_action( 'edit_user_profile', array(&$this, "__cb_umbx_display") );

		add_action( 'personal_options_update', array(&$this, "__cb_umbx_save") );
		add_action( 'edit_user_profile_update', array(&$this, "__cb_umbx_save") );
	}

	private function _bindValues($user){
		foreach ($this->_metas as $meta) {
			$meta->addActionAdminHead();
			$value = get_user_meta($user->data->ID, $meta->getName(), true);
			if(!empty($value)){
				$meta->addAttr("value",$value);
			}
		}		
	}

	public function __cb_umbx_display($user){

		if($user->roles && is_array($user->roles) && !in_array($this->_role->getID(), $user->roles))
			return;

		add_action("admin_footer", array(&$this, "__cb_umbx_head"));

		$this->_bindValues($user);

		ob_start();
		include ( $this->_template );
		$template = ob_get_contents();
		ob_end_clean();
		echo $template;
	}

	public function __cb_umbx_head(){
		?>
		<script type="text/javascript">
			jQuery(document).ready(function(){

				var $nav = jQuery(".nav-tab-wrapper"),
					$form = jQuery("#your-profile");

				jQuery(document).on("click", ".nav-tab", user_tab_onClick);

				if($nav.length == 0){
					$nav = jQuery('<h2 class="nav-tab-wrapper"></h2>');
					$nav.insertBefore("#your-profile");
					
					var $user_profile = jQuery('<div id="user-profile" class="tab-content"></div>');

					$user_profile.append($form.html());
					$form.html($user_profile);
					$form.append($user_profile.find(".submit"));

					$nav.append('<a href="#user-profile" class="nav-tab nav-tab-active">'+$form.find("h3:first").html()+'</a>');

				}

				$nav.append('<a href="#<?php echo $this->_id; ?>" class="nav-tab"><?php echo $this->_title; ?></a>');
				jQuery("#<?php echo $this->_id; ?>").addClass("tab-content").insertBefore("form .submit");

			});

			function user_tab_onClick(event){
				event.preventDefault();
				jQuery(".tab-content").hide();
				jQuery(jQuery(this).attr("href")).show();
				jQuery(this).addClass("nav-tab-active").siblings().removeClass("nav-tab-active");
			}
		</script>
		<?php
	}

	public function __cb_umbx_save($user_id){
		if($user_id && $_POST){
			foreach($this->_metas as $meta){
				if(isset($_POST[$meta->getName()])){
					update_user_meta( $user_id, $meta->getName(), $_POST[$meta->getName()] );
				}
			}
		}
	}
}