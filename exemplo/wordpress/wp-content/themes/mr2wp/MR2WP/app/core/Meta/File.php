<?php
class MR2WP_Meta_File extends MR2WP_Meta {

	public function __construct($name, $label, $attrs = array()){

		$this->_template = "File.php";
		$this->addHeadCallBack(array(&$this, 'adminHead'));

		parent::__construct($name, $label, $attrs);

	}

	/**
	 * Call back acionado pela action admin_head
	 */
	public function adminHead(){
		?>
		<script type="text/javascript">
			jQuery(document).ready(function(){

				jQuery('.custom_upload_file_button').click(function() {
					formfield = jQuery(this).siblings('.custom_upload_file');
					preview = jQuery(this).siblings('.custom_preview_file');
					tb_show('', 'media-upload.php?type=file&TB_iframe=true');
					window.send_to_editor = function(html) {
						fileurl = jQuery(html).attr("href");
					   	formfield.val(fileurl);

						preview.val(fileurl);
						tb_remove();
					}
					return false;
				});

				jQuery('.custom_clear_file_button').click(function() {
					var defaultImage = jQuery(this).parent().siblings('.custom_default_file').text();
					jQuery(this).parent().siblings('.custom_upload_file').val('');
					jQuery(this).parent().siblings('.custom_preview_file').val('');
					return false;
				});
			});
		</script>
		<?php
	}

}