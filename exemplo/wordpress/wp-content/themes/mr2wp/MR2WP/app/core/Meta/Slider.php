<?php
class MR2WP_Meta_Slider extends MR2WP_Meta {

	public function __construct($name, $label, $attrs = array(), $options = array()){

		$this->_template = "Slider.php";
		$this->addHeadCallBack(array(&$this, 'adminHead'));
		parent::__construct($name, $label, $attrs, $options);

	}

	/**
	 * Call back acionado pela action admin_head
	 */
	public function adminHead(){

		wp_enqueue_script('jquery-ui-slider');
		wp_enqueue_style('jquery-style', 'http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css');

		?>
		<script type="text/javascript">
			jQuery(document).ready(function(){
				jQuery( "#<?php echo $this->getName(); ?>-slider" ).slider({

					<?php
						foreach( $this->getOptions() as $option => $value)
							printf("%s: %s,", $option, $value);
					?>

					value: <?php echo $this->getAttr("value", 0); ?>,
					slide: function( event, ui ) {
						jQuery( "#<?php echo $this->getName(); ?>" ).val( ui.value );
					}
				});

			});
		</script>
		<?php
	}

}