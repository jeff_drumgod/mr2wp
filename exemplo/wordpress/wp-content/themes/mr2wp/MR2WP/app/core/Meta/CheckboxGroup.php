<?php
class MR2WP_Meta_CheckboxGroup extends MR2WP_Meta {

	public function __construct($name, $label, $attrs = null){

		$this->_template = "CheckboxGroup.php";

		parent::__construct($name, $label, $attrs);

	}

	/**
	 *
	 * @param MR2WP_Meta_Option $option
	 * @return MR2WP_Meta_CheckboxGroup
	 */
	public function addOption( $checkbox ){

		$this->_options[] = $checkbox;

		return $this;

	}

	/**
	 *
	 * @return array:MR2WP_Meta_Checkbox
	 */
	public function getOptions(){
		return $this->_options;
	}

}