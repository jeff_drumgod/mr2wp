<?php
/**
 * Classe que auxilia na criação de páginas no administrador do CMS.
 *
 * @author Marcel Rodrigo
 * @version 1.0
 * @date 2013-09-15
 * @category Wordpress
 * @link http://mr2wp.marcelrodrigo.com.br
 * @package MR2WP
 *
 */
class MR2WP_Admin_Page {

	private $page_title;
	private $menu_title;
	private $capability;
	private $menu_slug;
	private $callback_function;
	private $icon;
	private $position;
	private $parent;
	private $filePath;
	private $page_hook;

	private $actions = array();
	private $scripts = array();
	private $styles = array();


	/**
	* Classe que auxilia na criação de páginas no administrador do CMS.
	* @param string $page_title - Título da página.
	* @param string $menu_title - Título do item no menu.
	*/
	public function __construct($page_title, $menu_title){
		$this->page_title = $page_title;
	    $this->menu_title = $menu_title;
	    $this->menu_slug = sanitize_title($menu_title);
	}

	/**
	* Retorna o slug da página.
	* @return string
	*/
	public function getSlug()
	{
	    return $this->menu_slug;
	}

	/**
	* Adiciona a capacidade do grupo do usuário para acessar a página
	* @param string $capability - Nome da capacidade.
	* @return MR2WP_Admin_Page
	*/
	public function setCapability($capability)
	{
	    $this->capability = $capability;
	    return $this;
	}

	/**
	* Adiciona um template para a página.
	* @param string $path_template - Caminho físico do arquivo .php com conteúdo da página.
	* @return MR2WP_Admin_Page
	*/
	public function setTemplate($path_template){
		$this->callback_function = array($this, "cb_include_file");
	    $this->filePath = $path_template;

	    return $this;
	}

	/**
	* Adiciona um callback para gerar conteúdo na página.
	* @param string $callback - Nome da função para callback.
	* @return MR2WP_Admin_Page
	*/
	public function setCallback_function($callback)
	{

	    if( function_exists($callback) ){
	    	$this->callback_function = $callback;
		}

	    return $this;
	}

	/**
	* Adiciona um ícone para a página no menu.
	* @param string $icon_url - URL do ícone.
	* @return MR2WP_Admin_Page
	*/
	public function setIcon($icon_url)
	{
	    $this->icon = $icon;
	    return $this;
	}

	/**
	* Insere o link do menu em uma posição específica.
	* @param int $position - Posição do item no menu.
	* @return MR2WP_Admin_Page
	*/
	public function setPosition($position)
	{
	    $this->position = $position;
	    return $this;
	}

	/**
	* Adiciona a página como submenu de um menu existente.
	* @param string $menu_slug - Slug do menu onde este item será incluído.
	* @return MR2WP_Admin_Page
	*/
	public function setParent($parent)
	{
	    $this->parent = $parent;
	    return $this;
	}

	/**
	* Adiciona um script na página administrativa. Os scripts são incluídos com a função wp_enqueue_script.
	* @param string $name - Nome do script.
	* @param string $url (opcional) - Opcional. URL do script, caso o script já esteja registrado este parâmetro é opcional.
	* @return MR2WP_Admin_Page
	*/
	public function addScript($name, $url = null)
	{
		$this->scripts[$name] = $url;
		return $this;
	}

	/**
	* Adiciona uma folha de estilos CSS na página administrativa. Os stylesheets são incluídos com a função wp_enqueue_style.
	* @param string $name - Nome do stylesheet.
	* @param string $url (opcional) - Opcional. URL do stylesheet, caso o stylesheet já esteja registrado este parâmetro é opcional.
	* @return MR2WP_Admin_Page
	*/
	public function addStyle( $name, $url = null )
	{
		$this->styles[$name] = $url;
		return $this;
	}

	/**
	* Adiciona uma função de callback no hook da página.
	* @param string $callback - Função para ser executada.
	* @return MR2WP_Admin_Page
	*/
	public function addAction($callback)
	{
		$this->actions[] = $callback;
		return $this;
	}

	/**
	* Salva a página.
	*/
	public function save()
	{

		add_action("admin_menu", array(&$this, "cb_admin_menu"));
	}

	/********************************************************************************************/
	/*** CALL BACKS
	/********************************************************************************************/

	public function cb_admin_menu(){

		if( $this->parent ){
			$this->page_hook = add_submenu_page($this->parent, $this->page_title, $this->menu_title, $this->capability, $this->menu_slug, $this->callback_function);
		} else {
			$this->page_hook = add_menu_page($this->page_title, $this->menu_title, $this->capability, $this->menu_slug, $this->callback_function, $this->icon, $this->position);
		}

		add_action("admin_head-{$this->page_hook}", array(&$this, "cb_admin_head"));

		if(!empty($this->actions)){
			foreach($this->actions as $callback){
				add_action("load-{$this->page_hook}", $callback);
			}
		}
	}

	public function cb_include_file(){

		if( file_exists($this->filePath) ){
			include $this->filePath;
		}

	}

	public function cb_admin_head()
	{

		foreach( $this->scripts as $handle => $src )
		{
			wp_enqueue_script($handle, $src);
		}

		foreach( $this->styles as $handle => $src )
		{
			wp_enqueue_style($handle, $src);
		}

	}

}
