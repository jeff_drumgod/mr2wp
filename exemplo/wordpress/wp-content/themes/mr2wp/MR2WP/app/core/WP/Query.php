<?php
class MR2WP_WP_Query {
	
	public function join($join) {
		global $wp_query, $wpdb;
	
		if (!empty($wp_query->query_vars['s'])) {
			$join .= "LEFT JOIN $wpdb->postmeta ON $wpdb->posts.ID = $wpdb->postmeta.post_id ";
		}
	
		return $join;
	}
	
}