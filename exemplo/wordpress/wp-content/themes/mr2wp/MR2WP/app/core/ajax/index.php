<?php

if( isset( $_GET["action"] ) ){
	
	$file = dirname(__FILE__) . "/" . $_GET["action"] . ".php";
	
	if( file_exists($file) ){
		
		if(!defined("ABSPATH")){
		
			define('WP_USE_THEMES', false);
			require(dirname(__FILE__) . '/../../../../../../wp-blog-header.php');
		
		}
		
		include $file;
		
	} else {
		
		$result = new stdClass();
		
		$result->result = "error";
		$result->errors = array( "Action not found." );
		
		echo json_encode($result);
		
	}
	
}