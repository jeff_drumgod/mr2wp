<?php
class MR2WP_WP_Rewrite {
	
	private $rules;
	
	function cd_add_rewrites($content)
	{
		global $wp_rewrite;
		
		$wp_rewrite->non_wp_rules = array_merge($wp_rewrite->non_wp_rules, $this->rules);
		return $content;
	}
	
	function cd_flush_rewrites()
	{
		global $wp_rewrite;
		$wp_rewrite->flush_rules();
	}
	
	public function addRule( $alias, $src )
	{	
		$this->rules[$alias] = $src;
		return $this;	
	}
	
	public function save()
	{
		add_action('admin_init', array(&$this, 'cd_flush_rewrites'));
	
		if (!is_multisite() && !is_child_theme())
		{
			add_action('generate_rewrite_rules', array(&$this, 'cd_add_rewrites'));
		}
	}
	
}