<?php
/**
 * Classe auxiliar para reescrita de URLs.
 *
 * @author Marcel Rodrigo
 * @version 1.0
 * @date 2012-12-17
 * @category Wordpress
 * @link http://mr2wp.marcelrodrigo.com.br
 * @package MR2WP
 *
 */
class MR2WP_Rewrite {
	
	private $rules;	
	
	/**
	 * Adiciona uma regra de reescrita.
	 * @param string $alias - Nova URL.
	 * @param string $src - URL de destino.
	 * @return MR2WP_Rewrite
	 */
	public function addRule( $alias, $src )
	{	
		$this->rules[$alias] = $src;
		return $this;	
	}
	
	/**
	* Salva as regras criadas.
	*/
	public function save()
	{
		add_action('admin_init', array(&$this, 'cd_flush_rewrites'));
	
		if (!is_multisite() && !is_child_theme())
		{
			add_action('generate_rewrite_rules', array(&$this, 'cd_add_rewrites'));
		}
	}
	
	/********************************************************************************************/
	/*** CALL BACKS
	/********************************************************************************************/

	public function cd_add_rewrites($content)
	{
		global $wp_rewrite;
		
		$wp_rewrite->non_wp_rules = array_merge($wp_rewrite->non_wp_rules, $this->rules);
		return $content;
	}
	
	public function cd_flush_rewrites()
	{
		global $wp_rewrite;
		$wp_rewrite->flush_rules();
	}
}