<?php
class MR2WP_Meta_RadioGroup extends MR2WP_Meta {

	public function __construct($name, $label, $attrs = array()){

		$this->_template = "RadioGroup.php";

		parent::__construct($name, $label, $attrs);

	}

	/**
	 *
	 * @param MR2WP_Meta_Option $option
	 * @return MR2WP_Meta_RadioGroup
	 */
	public function addOption( $option ){

		$this->_options[] = $option;

		return $this;

	}

	/**
	 *
	 * @return array:MR2WP_Meta_Option
	 */
	public function getOptions(){
		return $this->_options;
	}

}