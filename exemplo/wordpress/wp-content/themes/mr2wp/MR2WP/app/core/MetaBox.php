<?php
/**
 * A classe MR2WP_MetaBox facilita a manipulação de post meta boxes.
 *
 * @author Marcel Rodrigo
 * @version 1.0
 * @date 2013-01-07
 * @category Wordpress
 * @link http://mr2wp.marcelrodrigo.com.br
 * @package MR2WP
 *
 */
class MR2WP_MetaBox {

	private $_id;
	private $_title;
	private $_posttype;
	private $_context;
	private $_priority;
	private $_metas = array();
	private $_template;

	/**
	* Cria uma nova instancia de MR2WP_MetaBox.
	* @param string $id - Identificador do meta box
	* @param string $title - Título do meta box. Default: Opções
	* @param string $priority - Prioridade do metabox. Default:  default
	* @param string $context - Contexto do meta box. Default: advanced
	*/
	public function __construct( $id, $title = 'Opções', $priority = 'default', $context = 'advanced' ){

		$this->_id = $id;
		$this->_title = $title;
		$this->_context = $context;
		$this->_priority = $priority;

		$this->setTemplate("MetaBoxTable.php");
	}

	/**
	 * Retorna o id do metabox
	 * @return string
	 */
	public function getID(){
		return $this->_id;
	}

	/**
	 * Retorna todos os post meta adicionados ao meta box
	 * @return array: MR2WP_Meta
	 */
	public function getMetas(){
		return $this->_metas;
	}

	/**
	 * Retorna o post type a que pertence
	 * @return MR2WP_PostType
	 */
	public function getPostType(){
		return $this->_posttype;
	}

	/**
	 * Define o post type onde o meta box deve aparecer
	 * @param MR2WP_PostType $posttype
	 * @return MR2WP_MetaBox
	 */
	public function setPostType( MR2WP_PostType $posttype ){
		$this->_posttype = $posttype;
		return $this;
	}

	/**
	 * Adiciona um post meta ao meta box
	 * @param MR2WP_Meta $meta
	 * @return MR2WP_MetaBox
	 */
	public function addMeta( MR2WP_Meta $meta ){
		$this->_metas[] = $meta;
		return $this;
	}

	/**
	 * Define o template do meta box
	 * @param string $template
	 * @return MR2WP_MetaBox
	 */
	public function setTemplate($template){
		$this->_template = dirname(__FILE__) . "/Templates/".$template;

		return $this;
	}

	/**
	 * Salva o metabox
	 */
	public function save(){
		add_action('add_meta_boxes', array(&$this, 'cb_add_metabox'));
		add_action('save_post', array(&$this, 'cb_save_post'));
	}

	public function bindValues($post_id){

		global $post;

		foreach( $this->_metas as $meta){

			if($post && $post->post_type == $this->_posttype->getName()){
				$meta->addActionAdminHead();
			}
			$value = get_post_meta($post_id, $meta->getName(), true);

			if($value){

				$meta->addAttr("value",$value);

			}
		}
	}

	public function render( $post ) {

		$this->bindValues($post->ID);

		ob_start();
		include ( $this->_template );
		$template = ob_get_contents();
		ob_end_clean();
		echo $template;

	}

	/********************************************************************************************/
	/*** CALL BACKS
	/********************************************************************************************/

	/**
	 * Callback invocado pela action add_meta_boxes.
	 * Registra o metabox.
	 */
	public function cb_add_metabox(){

		add_meta_box( $this->_id, $this->_title, array(&$this, 'render'), $this->_posttype->getName(), $this->_context, $this->_priority );

	}

	/**
	 * Callback acionado pela action save_post. Salva os valores de meta adicionados ao post.
	 * @param int $post_id - ID do post
	 */
	public function cb_save_post($post_id){

		// verify nonce
		//if (!wp_verify_nonce($_POST[$this->_id.'_nonce'], basename(__FILE__))) return $post_id;

		// check autosave
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return $post_id;

		// check permissions
		if ('page' == $_POST['post_type']) {
			if (!current_user_can('edit_page', $post_id))
				return $post_id;
		} elseif (!current_user_can('edit_post', $post_id)) {
			return $post_id;
		}

		foreach($this->_metas as $meta){

			$old = get_post_meta($post_id, $meta->getName(), true);
			$new = $_POST[$meta->getName()];

			update_post_meta($post_id, $meta->getName(), $new);

			if($meta->getName() == "cores"){
				var_dump($new);
			}

			// if ($new && $new != $old) {
			// 	update_post_meta($post_id, $meta->getName(), $new);
			// } elseif ('' == $new && $old) {
			// 	delete_post_meta($post_id, $meta->getName(), $old);
			// }

		}

	}

}