<?php
/**
 * A MR2WP_Meta_Multimida abstrai as metas de multimidia.
 * Esta é uma classe abstrata, por isso não pode ser instanciada, pois não representa nenhum elemento visual. Para fazer uso de suas funcionalidade é preciso extendê-la.
 * Esta classe tem dependência da classe MR2WP_PostType e MR2WP_Taxonomy
 * @author Marcel Rodrigo
 * @version 2.0
 * @date 2013-12-28
 * @category Meta
 * @link http://mr2wp.marcelrodrigo.com.br
 * @package MR2WP
 */
abstract class MR2WP_Meta_Multimidia extends MR2WP_Meta {

	private $_post_type;

	protected $_defaults = array();

	public function __construct($name, $label, $attrs = array()){

		add_theme_support( 'post-formats', array( 'gallery', 'video', 'image' ) );

		// nome do post type que armazenará as mídias
		$this->_defaults["post_type"] 		= "multimidia";
		$this->_defaults["post_type_label"] = "Multimídia";

		// nome da taxonomia que irá separar as mídias por meta
		$this->_defaults["tax_name"] 		= "meta";
		$this->_defaults["tax_label"]		= "Meta";

		$this->_register();

		parent::__construct($name, $label, $attrs);
	}

	private function _register(){


		$this->_post_type		= new MR2WP_PostType($this->_defaults["post_type"]);
		$this->_post_type
			->setLabels(array(
				"name" 			=> $this->_defaults["post_type_label"],
				"singular_name" => $this->_defaults["post_type_label"],
				"menu_name" 	=> $this->_defaults["post_type_label"]
			))
			->setArgs(array(
				"menu_position" => 6,
				"rewrite" 		=> false,
				'hierarchical' 	=> false,
				'has_archive'	=> true,
				"supports" 		=> array( "title", "editor", "thumbnail", "tags", "post-formats" )
			))
			->save();

		$tax = new MR2WP_Taxonomy($this->_defaults["tax_name"]);
		$tax
			->setArgs(array(
				"label" 		=> $this->_defaults["tax_label"],
				'show_ui' 		=> false,
			))
			->setPostType($this->_post_type)
			->showInColumn()
			->showInFilter()
			->save();

		add_action("init", array(&$this, "__cb_register_term"));
		add_action("after_setup_theme", array(&$this, "__cb_after_setup_theme"));
	}

	public function __cb_after_setup_theme(){
		add_post_type_support( $this->_defaults["post_type"], "post-formats" );
	}

	public function __cb_register_term(){
		$terms = get_terms($this->_defaults["tax_name"], array(
			"hide_empty" 	=> false,
			"fields"		=> "names"
		));

		if(!in_array($this->getLabel(), $terms)){
			wp_insert_term(
				$this->getLabel(),
				$this->_defaults["tax_name"],
				array(
					"description" 	=> $this->getOption("description") ? $this->getOption("description") : "",
					"slug" 			=> $this->getName(), 
				)
			);
		}
	}

}