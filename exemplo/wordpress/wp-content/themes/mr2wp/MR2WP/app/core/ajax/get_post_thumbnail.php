<?php

$json = new stdClass();
$json->errors = array();

if( isset($_GET["post_id"])){

	$size = isset($_GET["size"]) ? $_GET["size"] : array(180, 160);
	$post_id = $_GET["post_id"];
	$attr = isset($_GET["attr"]) ? $_GET["attr"] : "";
	
	$image = wp_get_attachment_image_src($post_id, $size);

	if($image){
		
		$json->result = "success";
		$json->url = $image[0];
		$json->id = $post_id;
		
	} else {
		
		$json->result = "error";
		$json->errors[] = "Image not found.";
		
	}
	
	
	
} else {
	
	$json->result = "error";
	$json->errors[] = "Post id invalid.";
	
}

echo json_encode($json);