<?php
/**
 * A classe MR2WP_Role facilita a manipulação de user roles.
 *
 * @author Marcel Rodrigo
 * @version 1.0
 * @date 2013-10-24
 * @category Wordpress
 * @link http://mr2wp.marcelrodrigo.com.br
 * @package MR2WP
 *
 */
class MR2WP_User_Role {

	private $_role;
	private $_name;
	private $_capabilities = array();
	private $_login_rediect_url;
	private $_fields_to_remove = array();

	private $_oRole;

	/**
	* Cria uma nova instância de MR2WP_Role
	* @param $role - Nome da role.
	* @param $name - Nome que será exibido na interface.
	*/
	public function __construct($role, $name){
		$this->_role = $role;
		$this->_name = $name;

		$this->_save();
	}

	/**
	* Adiciona um grupo de capabilities, capability type para a role.
	* @param MR2WP_CapabilityType $capability_type - Instância de MR2WP_CapabilityType
	* @return MR2WP_Role
	*/
	public function addCapabilityType(MR2WP_CapabilityType $capability_type){
		foreach($capability_type->getCapabilities() as $cap){
			$this->addCapability($cap);
		}
		
		return $this;
	}

	/**
	* Adiciona uma capability para a role.
	* @param $capability - Nome da capability
	* @return MR2WP_Role
	*/
	public function addCapability($capability){
		$this->_capabilities[] = $capability;

		$this->_oRole->add_cap($capability);

		return $this;
	}

	/**
	* Retorna o id da role;
	* @return string - ID da role.
	*/
	public function getID(){
		return $this->_role;
	}

	/**
	* Retorna o nome da role
	* @return string - Nome da role.
	*/
	public function getName(){
		return $this->_name;
	}

	/**
	* Define uma página de redirecionamento após o login.
	* @param $url - URL de destino.
	* @return MR2WP_Role
	*/
	public function loginRedirect($url){
		$this->_login_rediect_url = $url;
		add_filter("login_redirect", array($this, "__cb_rl_login_redirect"), 10, 3);
		return $this;
	}

	/**
	* Remove campos padrão do perfil do usuário.
	* @param string $field - Nome do campo.
	* @return MR2WP_Role
	*/
	public function removeField($field){
		$this->_fields_to_remove[] = $field;

		if(count($this->_fields_to_remove) == 1){
			add_filter('user_contactmethods', array(&$this, "__cb_rl_remove_fields"));
		}

		return $this;
	}

	private function _save(){
		add_role( $this->_role, $this->_name);
		$this->_oRole = get_role( $this->_role );
	}

	/********************************************************************************************/
	/*** CALL BACKS
	/********************************************************************************************/
	
	public function __cb_rl_login_redirect($redirect_to, $request, $user){
		if($user && is_object($user) && is_a($user, "WP_User") && $this->_login_rediect_url){
			return $this->_login_rediect_url;
		}

		return $redirect_to;
	}

	public function __cb_rl_remove_fields($profile_fields){
		global $user_id;

		$user = new WP_User( $user_id );
		if ( $user->roles && is_array( $user->roles ) && in_array($this->_role, $user->roles) ) {
			foreach($this->_fields_to_remove as $field){
				unset($profile_fields[$field]);
			}	
		}
		
		return $profile_fields;
	}

}