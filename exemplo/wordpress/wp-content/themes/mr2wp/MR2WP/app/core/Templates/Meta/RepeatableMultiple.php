<?php 
	$values = $this->getValue();

	$controls = $this->getControls();
	
?>

<a rel="<?php echo $this->getName(); ?>-repeatable" class="repeatable-add button" href="#">+</a>

<ul id="<?php echo $this->getName(); ?>-repeatable" class="custom_repeatable">
	<?php

		if($values) : 

			for( $i = 0 , $s = count($values[$controls[0]->getName()]); $i < $s; $i++ ): 
	?>
	<li>
		<!-- <span class="sort hndle">|||</span> -->
		<?php 
			
			foreach($controls as $controll){
				
				$args = $controll->getAttrs();
				$args['value'] = $values[$controll->getName()][$i];
				
				$class = get_class($controll);
				$newControll = new $class($controll->getName(), $controll->getLabel(), $args);
				
				printf("<label for=\"%s\">%s</label>%s<br />", $controll->getName(), $controll->getLabel(), $newControll->display());
				
			}
			
		?>
		
		<a class="repeatable-remove button" href="#">-</a>
	</li>
	<?php 
			endfor; 
		else:
	?>
	<li>
		<!-- <span class="sort hndle">|||</span> -->
		<?php 
			foreach( $this->getControls() as $controll ){
				printf("<label for=\"%s\">%s</label>%s<br />", $controll->getName(), $controll->getLabel(), $controll->display());
			}
		?>
		<a class="repeatable-remove button" href="#">-</a>
	</li>
	<?php endif; ?>
</ul>