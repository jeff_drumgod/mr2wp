<?php wp_nonce_field(plugin_basename(__FILE__), 'wp_custom_attachment_nonce'); ?>

<span class="custom_default_image" style="display:none"></span>
<img src="<?php echo $this->getValue(); ?>" class="custom_preview_image" alt="" /><br />

<input name="<?php echo $this->getName(); ?>" type="hidden" class="custom_upload_image" value="<?php echo $this->getValue(); ?>" />
<input class="custom_upload_image_button button" type="button" name="button" value="Selecione a imagem" />
<small>&nbsp;<a href="#" class="custom_clear_image_button">Remover imagem</a></small>