<?php
class MR2WP_Meta_SelectDynamic extends MR2WP_Meta_Select {
	
	private $_dependency;
	private $_url;
	private $_data;
	
	public function __construct($name, $label, $attrs = array()){
		
		$this->_template = "SelectSelect.php";
		$this->addHeadCallBack(array(&$this, 'adminHead'));
		
		parent::__construct($name, $label, $attrs);
		
	}
	
	/**
	 * 
	 * @param string $query_string
	 * @return MR2WP_Meta_SelectDynamic
	 */
	public function setAjaxUrl( $url ){
		
		$this->_url = $url;
		
		return $this;
		
	}
	
	/**
	 * Set data to ajax call.
	 * @param array $data
	 * @return MR2WP_Meta_SelectDynamic
	 */
	public function setData( array $data ){
		
		$this->_data = $data;
		
		return $this;	
		
	}
	
	/**
	 * 
	 * @param MR2WP_Meta $meta
	 * @return MR2WP_Meta_SelectDynamic
	 */
	public function setDependency( $meta ){
		
		$this->_dependency = $meta;
		
		return $this;
		
	}
	
	/**
	 * Call back acionado pela action admin_head
	 */
	public function adminHead(){
		
		?>
		
		<script type="text/javascript">

			jQuery(document).ready(function(){

				var $this = jQuery("#<?php echo $this->getName() ?>");
				var $dependency = jQuery("#<?php echo $this->_dependency->getName(); ?>");
				
				$dependency.change(function( e ){

					var selected = jQuery(this).find("option:selected");
					var data = <?php echo json_encode($this->_data); ?>;
					var dataString = "";
					
					data = jQuery.map(data, function( value, key ){
						return key + "=" + value;
					});
				
					data = data.join("&");
					data = data.replace("?", selected.val());
					
					jQuery.ajax({
						url: "<?php echo $this->_url; ?>",
						data: data,
						dataType: "json",
						success: select_dynamic_on_success
					});
					
				});

				if( !$this.children().length > 0 && $dependency.children().length > 0 ){

					$dependency.trigger("change");
			
				}

				function select_dynamic_on_success( data ){

					$this.html("");
					
					if( data.result == "success" ){

						if( data.post_count > 0 ){

							for( i in data.posts ){

								var $opt = jQuery("<option value=\""+data.posts[i].ID+"\">"+data.posts[i].post_title+"</option>");

								$this.append($opt);
								
							}
							
						}

					} else {

						// erro
						
					}
					
				}
				
			});
			
		</script>
		
		<?php
		
	}
	
}