<?php wp_nonce_field(plugin_basename(__FILE__), 'wp_custom_attachment_nonce'); ?>

<input name="<?php echo $this->getName(); ?>" id="<?php echo $this->getName(); ?>" type="hidden" class="custom_upload_file" value="<?php echo $this->getValue(); ?>" />
<input name="custom_preview_file" readonly="readonly" type="text" class="custom_preview_file" value="<?php echo $this->getValue(); ?>" />
<input class="custom_upload_file_button button" type="button" name="button" value="Escolha um arquivo" />
<small>&nbsp;<a href="#" class="custom_clear_file_button">Remova o arquivo</a></small>