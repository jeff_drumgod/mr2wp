<?php
/**
 * Classe auxiliar para manipulação de PostType
 *
 * @author Marcel Rodrigo
 * @version 1.4
 * @date 2012-12-17
 * @category Wordpress
 * @link http://mr2wp.marcelrodrigo.com.br
 * @package MR2WP
 *
 */
class MR2WP_PostType {

	private $_name;

	private $_args = array(
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'query_var' => true,
		'menu_position' => 4,
		'capability_type' => 'post',
		'hierarchical' => true,
		'has_archive' => true,
		'rewrite' => array('slug' => '', 'with_front' => true, 'hierarchical' => true ),
		'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt', 'tags' ),
	);

	private $_labels = array(
		'name' => '',
		'singular_name' => '',
		'add_new' => 'Adicionar item',
		'add_new_item' => 'Adicionar item',
		'edit_item' => 'Editar item',
		'new_item' => 'Novo item',
		'all_items' => 'Todos os itens',
		'view_item' => 'Ver itens',
		'search_items' => 'Buscar itens',
		'not_found' =>  'Nenhum item encontrado',
		'not_found_in_trash' => 'Nenhum item na lixeira',
		'parent_item_colon' => '',
		'menu_name' => ''
	);

	/**
	* Cria uma instância de MR2WP_PostType
	* @param $name - Nome do post type
	*/
	public function __construct( $name ){

		$this->_name = $name;
		$this->_args["labels"] = &$this->_labels;

	}

	/**
	 * Retorna o nome do post type.
	 * @return string
	 */
	public function getName(){

		return $this->_name;

	}

	/**
	 * Retorna um label do post type
	 * @return string
	 */
	public function getLabel($label){
		return $this->_labels[$label];
	}

	/**
	 * Define os labels do post type.
	 * @param array $labels
	 * @return MR2WP_PostType
	 * @link http://codex.wordpress.org/Function_Reference/register_post_type#Arguments
	 */
	public function setLabels ( $labels ){

		$this->_labels = array_merge($this->_labels, $labels);

		return $this;

	}

	/**
	 * Define as propriedades do post type
	 * @param array $args
	 * @return MR2WP_PostType
	 * @link http://codex.wordpress.org/Function_Reference/register_post_type#Arguments
	 */
	public function setArgs( $args ){

		$this->_args = array_merge($this->_args, $args);

		return $this;

	}

	/**
	 * Salva o post type.
	 * @return boolean
	 */
	public function save(){

		if( !post_type_exists( $this->_name ) ){
			add_action('init', array(&$this, '__cb_pt_register'));
		} else {
			return false;
		}

		return true;

	}

	/********************************************************************************************/
	/*** CALL BACKS
	/********************************************************************************************/

	/**
	 * Callback invocado pela action init.
	 * Registra o post type.
	 */
	public function __cb_pt_register(){
		register_post_type($this->_name, $this->_args);
	}


}