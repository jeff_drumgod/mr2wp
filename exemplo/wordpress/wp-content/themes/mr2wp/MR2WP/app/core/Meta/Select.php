<?php
class MR2WP_Meta_Select extends MR2WP_Meta {
	
	public function __construct($name, $label, $attrs = array()){
		
		$this->_template = "Select.php";
		
		parent::__construct($name, $label, $attrs);
		
	}
	
	/**
	 * 
	 * @param MR2WP_Meta_Option $option
	 * @return MR2WP_Meta_Select
	 */
	public function addOption( $option ){
		
		$this->_options[] = $option;
		
		return $this;
		
	}
	
	/**
	 * 
	 * @return array:MR2WP_Meta_Option
	 */
	public function getOptions(){
		return $this->_options;
	}
	
}