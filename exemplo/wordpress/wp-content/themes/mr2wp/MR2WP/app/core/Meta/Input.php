<?php
class MR2WP_Meta_Input extends MR2WP_Meta {

	public function __construct($name, $label, $attrs = array()){

		$this->_template = "Input.php";

		if(!isset($attrs["type"])){
			$attrs["type"] = "text";
		}

		parent::__construct($name, $label, $attrs);

	}

}