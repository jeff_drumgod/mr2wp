<?php
class MR2WP_Meta_Gallery extends MR2WP_Meta {

	public function __construct($name, $label){

		$this->_template = "Gallery.php";
		$this->addHeadCallBack(array(&$this, 'adminHead'));

		parent::__construct($name, $label, array());

	}

	/**
	 * Call back acionado pela action admin_head
	 */
	public function adminHead(){
		wp_enqueue_media();

		wp_enqueue_script("jquery-ui-sortable");

		global $post;

		?>
		<script type="text/javascript">

		jQuery(document).ready(function(){

			var $gallery_container = jQuery("#<?php echo $this->getName(); ?>-container");

			// Uploading files
			var file_frame_<?php echo $this->getName(); ?>;
			var wp_media_post_id = wp.media.model.settings.post.id; // Store the old id
			var set_to_post_id = <?php echo $post->ID; ?>; // Set this

			jQuery('#btn-<?php echo $this->getName(); ?>').on('click', function( event ){

				event.preventDefault();
				var $this = jQuery(this);

				// If the media frame already exists, reopen it.
				if ( file_frame_<?php echo $this->getName(); ?> ) {
					 // Set the post ID to what we want
					file_frame_<?php echo $this->getName(); ?>.uploader.uploader.param( 'post_id', set_to_post_id );

					// Open frame
					file_frame_<?php echo $this->getName(); ?>.open();
					return;
				} else {
					// Set the wp.media post id so the uploader grabs the ID we want when initialised
					wp.media.model.settings.post.id = set_to_post_id;
				}

				 // Create the media frame.
				file_frame_<?php echo $this->getName(); ?> = wp.media.frames.file_frame_<?php echo $this->getName(); ?> = wp.media({

					title: jQuery( this ).data( 'uploader_title' ),
					button: {
						text: jQuery( this ).data( 'uploader_button_text' ),
					},
					multiple:     'add',
					frame:   'select',
					state:	 '<?php echo $this->getName(); ?>',
					library:   {type: 'image'}

				});

				file_frame_<?php echo $this->getName(); ?>.states.add([

                  	new wp.media.controller.Library({
                  		id:         '<?php echo $this->getName(); ?>',
                  		priority:   10,
                  		filterable: 'uploaded',
                  		library:    wp.media.query( file_frame_<?php echo $this->getName(); ?>.options.library ),
                  		multiple:   file_frame_<?php echo $this->getName(); ?>.options.multiple ? 'add' : false,
                  		editable:   true,
                  		sortable:   true,
                  		displayUserSettings: false,
                  		displaySettings: true,
                  		allowLocalEdits: true,
                  		menu:       'default'
                  	}),
				]);

				 // When an image is selected, run a callback.
				file_frame_<?php echo $this->getName(); ?>.on( 'select', function() {

					var selection = file_frame_<?php echo $this->getName(); ?>.state().get('selection');

					selection.map( function( attachment ) {

						attachment = attachment.toJSON();

						// Do something with attachment.id and/or attachment.url here

						 // Restore the main post ID
						wp.media.model.settings.post.id = wp_media_post_id;
					});

					var images_id = [];
					var images = [];

					for(i in selection.models){

						images_id[i] = selection.models[i].id;

						jQuery.ajax({
							url: "<?php echo MR2WP_URL; ?>/app/ajax/index.php",
							dataType: "json",
							data: "action=get_post_thumbnail&post_id=" + images_id[i],
							success: function( data ){

								if( data.result = "success" ){

									var $li = jQuery("<li></li>");
									var $img = jQuery("<img />");
									var $remove = jQuery("<a></a>");

									$img.attr("src", data.url);

									$remove.attr("href", "#");
									$remove.addClass("remove");

									$li.attr("id", data.id);
									$li.append($img).append($remove);
									$li.hide();

									$gallery_container.find("ul").append($li);
									$li.fadeIn();

									var $input = jQuery("<input type=\"hidden\" />");
									$input.attr("name", "<?php echo $this->getName(); ?>[]");
									$input.val(data.id);

									$gallery_container.append($input);

								} else {

									// erro

								}

							}
						});

						images_id[i] = selection.models[i].id;

					}

				});

				// Finally, open the modal
				file_frame_<?php echo $this->getName(); ?>.open();
			});

			jQuery(".custom_clear_gallery_button").on('click', function(e){

				e.preventDefault();

				jQuery("input[name=\"<?php echo $this->getName() ?>[]\"]").remove();

				$gallery_container.find("li").fadeOut();

			});

			$gallery_container.find(".remove").live("click", function( event ){

				event.preventDefault();

				var $li = jQuery(this).closest("li");
				var id = $li.attr("id");

				$li.fadeOut("fast", function(){

					$li.remove();

				});

				jQuery("input[value="+id+"]").remove();

			});

			$gallery_container.find("ul").sortable({
				beforeStop: function( event, ui ){

					jQuery("input[name=\"<?php echo $this->getName() ?>[]\"]").remove();

					$gallery_container.find("li:not(.ui-sortable-placeholder)").each(function(i){

						var $input = jQuery("<input type=\"hidden\" />");
						$input.attr("name", "<?php echo $this->getName(); ?>[]");
						$input.val(jQuery(this).attr("id"));

						$gallery_container.append($input);

					});

				}
			});

			$gallery_container.disableSelection();
		});

		</script>
		<?php
	}

}