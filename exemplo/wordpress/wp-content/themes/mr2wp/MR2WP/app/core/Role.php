<?php
/**
 * A classe MR2WP_Role facilita a manipulação de user roles.
 *
 * @author Marcel Rodrigo
 * @version 1.0
 * @date 2013-10-24
 * @category Wordpress
 * @link http://mr2wp.marcelrodrigo.com.br
 * @package MR2WP
 *
 */
class MR2WP_Role {

	private $_role;
	private $_name;
	private $_capabilities = array();
	private $_login_rediect_url;

	private $_oRole;

	/**
	* Cria uma nova instância de MR2WP_Role
	* @param $role - Nome da role.
	* @param $name - Nome que será exibido na interface.
	*/
	public function __construct($role, $name){
		$this->_role = $role;
		$this->_name = $name;

		$this->_save();
	}

	/**
	* Adiciona um grupo de capabilities, capability type para a role.
	* @param MR2WP_CapabilityType $capability_type - Instância de MR2WP_CapabilityType
	* @return MR2WP_Role
	*/
	public function addCapabilityType(MR2WP_CapabilityType $capability_type){
		foreach($capability_type->getCapabilities() as $cap){
			$this->addCapability($cap);
		}
		
		return $this;
	}

	/**
	* Adiciona uma capability para a role.
	* @param $capability - Nome da capability
	* @return MR2WP_Role
	*/
	public function addCapability($capability){
		$this->_capabilities[] = $capability;

		$this->_oRole->add_cap($capability);

		return $this;
	}

	/**
	* Define uma página de redirecionamento após o login.
	* @param $url - URL de destino.
	* @return MR2WP_Role
	*/
	public function loginRedirect($url){
		$this->_login_rediect_url = $url;
		add_filter("login_redirect", array($this, "__cb_rl_login_redirect"), 10, 3);
		return $this;
	}

	private function _save(){
		add_role( $this->_role, $this->_name);
		$this->_oRole = get_role( $this->_role );
	}

	/********************************************************************************************/
	/*** CALL BACKS
	/********************************************************************************************/
	
	public function __cb_rl_login_redirect($redirect_to, $request, $user){
		if($user && is_object($user) && is_a($user, "WP_User") && $this->_login_rediect_url){
			return $this->_login_rediect_url;
		}

		return $redirect_to;
	}

}