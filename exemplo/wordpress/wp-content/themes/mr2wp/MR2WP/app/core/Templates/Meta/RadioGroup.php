<?php if($this->getAttr("title")): ?>
<p><?php echo $this->getAttr("title"); ?></p>
<?php endif; ?>

<?php foreach ($this->getOptions() as $option): ?>

	<input 
		type="radio" 
		name="<?php echo $this->getName(); ?>" 
		id="<?php echo $this->getName(); ?>"
		value="<?php echo $option->getName(); ?>"
		<?php echo $this->getValue() == $option->getName() ? "checked=\"checked\"" : ""; ?>
		
		<?php 
			foreach($this->getAttrs() as $attr => $value){
			
				printf("%s=\"%s\" ", $attr, $value);
			 	
			}  
		?>
	/>
	
	<label for="<?php echo $option->getName(); ?>"><?php echo $option->getLabel(); ?></label> <br />

<?php endforeach; ?>