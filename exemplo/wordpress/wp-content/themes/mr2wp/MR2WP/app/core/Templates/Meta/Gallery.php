<div class="mr2wp-meta-gallery" id="<?php echo $this->getName(); ?>-container">

	<a href="#" id="btn-<?php echo $this->getName(); ?>" class="meta_gallery_button button button-primary">Adicionar fotos</a>
	<a href="#" class="custom_clear_gallery_button button">Remover fotos</a>
	
	<ul>
	<?php if($this->getValue()): foreach( $this->getValue() as $id ): ?> 
		<li id="<?php echo $id; ?>">
			<?php 
				$image = wp_get_attachment_image_src($id, array(180, 160));
				printf("<img src=\"%s\" />", $image[0]);
			?>
			<a href="#" class="remove"></a>
		</li>
	<?php endforeach; endif; ?>
	</ul>

	<div class="clear"></div>
	
	<?php if($this->getValue()): foreach( $this->getValue() as $id ): ?>
	<input type="hidden" name="<?php echo $this->getName(); ?>[]" value="<?php echo $id; ?>" />
	<?php endforeach; endif; ?>
	
</div>