<?php

$json = new stdClass();
$json->errors = array();

if( video_insert_validate() ){

	$url = esc_url($_POST["url"]);
	$title = sanitize_text_field($_POST["title"]);
	$description = isset($_POST["description"]) ? esc_textarea($_POST["description"]) : null;
	$author_id = isset($_POST["author_id"]) ? intval($_POST["author_id"]) : null;
	$post_type = $_POST["post_type"];
	$taxonomy = $_POST["taxonomy"];
	$meta = $_POST["meta"];

	$post_id =	wp_insert_post(
		array(
			"comment_status" 		=> "open",
			"ping_status" 			=> "open",
			"post_author" 			=> $author_id,
			"post_content" 			=> $url,
			"post_date" 			=> date("Y-m-d H:i:s"),
			"post_name"				=> sanitize_title($title),
			"post_status"			=> "publish",
			"post_title"			=> $title,
			"post_type" 			=> $post_type,
			"tax_input"				=> array( $taxonomy => array($meta) )
		)
	);

	if($post_id){
		set_post_format($post_id, "video");

		$video_meta = array();

		if($description)
			$video_meta["description"] = $description;

		if($video_meta)
			update_post_meta($post_id, "video_meta", $video_meta);

		$video_info = MR2WP_Util::get_video_info($url);

		MR2WP_Util::insertExteralAttachment($post_id, $video_info->thumbs["hq"], sanitize_title($title));

		$json->thumb_url = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), array(180, 160))[0];
		$json->result = true;
		$json->post_id = $post_id;
	} else {
		$json->result = false;
		$json->errors[] = "Post save failed.";
	}

} else {
	
	$json->result = false;
	$json->errors[] = "Missing parameters.";
	
}

echo json_encode($json);

function video_insert_validate(){
	global $json;
	$required = array("title", "url", "author_id", "post_type", "taxonomy", "meta");

	$valid = true;

	foreach($required as $key){
		if(!isset($_POST[$key])){
			$valid = false;
			$json->errors[] = "Missing parameter {$key}";
		}
	}

	return $valid;
}