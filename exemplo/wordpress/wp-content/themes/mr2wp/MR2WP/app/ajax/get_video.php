<?php

$json = new stdClass();
$json->errors = array();

if( isset($_GET["video_url"])){

	
	$video_info = MR2WP_Util::get_video_info($_GET["video_url"]);

	if($video_info){
		$json->result = true;
		$json->video_info = $video_info;
	}

	$json->video_info = $video_info;
	
	
} else {
	
	$json->result = "error";
	$json->errors[] = "Video url expected.";
	
}

echo json_encode($json);