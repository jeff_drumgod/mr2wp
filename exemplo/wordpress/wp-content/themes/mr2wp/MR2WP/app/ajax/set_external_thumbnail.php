<?php

$json = new stdClass();
$json->errors = array();

if( isset($_POST["image_url"]) && isset($_POST["post_id"]) ){

	$image_url = esc_url($_POST["image_url"]);
	$post_id = (int) $_POST["post_id"];

	MR2WP_Util::insertExteralAttachment($post_id, $image_url);

	$json->result = true;

} else {
	
	$json->result = false;
	$json->errors[] = "Invalid request.";
	
}

echo json_encode($json);