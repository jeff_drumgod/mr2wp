<?php get_header(); ?>

	<div class="wrap">
		<p>MR2WP é um framework que facilita no desenvolvimento de Temas e Plugins para Wordpress.</p>
		<p>É altamente extensível, possibilitando o desenvolvedor exterder funcionalidades para um melhor aproveitamento do CMS.</p>

		<h2>Classes</h2>
		<ul>
			<li><a href="#">MR2WP_Admin_Page</a></li>
			<li><a href="#">MR2WP_Role</a></li>
			<li><a href="#">MR2WP_CapabilityType</a></li>
			<li><a href="#">MR2WP_Rewrite</a></li>
			<li><a href="#">MR2WP_PostType</a></li>
			<li><a href="#">MR2WP_Taxonomy</a></li>
			<li><a href="#">MR2WP_MetaBox</a></li>
			<li><a href="#">MR2WP_Meta</a></li>
		</ul>

		<h2>MR2WP_Admin_Page</h2>
		<p>A classe MR2WP_Admin_Page pocibilita na criação de páginas no administrador do CMS.</p>
		<p>Veja esta funcionalidade nativa na <a href="http://codex.wordpress.org/Function_Reference/add_menu_page" target="_blank">documentação oficial</a>.</p>
		<dl>
			<dt>Métodos</dt>
			<dd>
				<h4>setCapability($capability)</h4>
				<p>Adiciona a capacidade do grupo do usuário para acessar a página (<a href="http://codex.wordpress.org/Roles_and_Capabilities‎"  target="_blank">Veja capabilities na documentação oficial do Wordpress.</a>)</p>
				<h5>Parâmetros</h5>
				<p><span class="name">$capability (string):</span> Nome da capacidade.</p>
				<h5>Retorno</h5>
				<p><span class="name">MR2WP_Admin_Page</span> Instância do objeto.</p>
			</dd>
			<dd>
				<h4>setTemplate($path_template)</h4>
				<p>Adiciona um template para a página.</p>
				<h5>Parâmetros</h5>
				<p><span class="name">$path_template (string):</span> Caminho físico do arquivo .php com conteúdo da página.</p>
				<h5>Retorno</h5>
				<p><span class="name">MR2WP_Admin_Page</span> Instância do objeto.</p>
			</dd>
			<dd>
				<h4>setCallback_function($callback)</h4>
				<p>Adiciona um callback para gerar conteúdo na página.</p>
				<h5>Parâmetros</h5>
				<p><span class="name">$callback (string):</span> Nome da função para callback.</p>
				<h5>Retorno</h5>
				<p><span class="name">MR2WP_Admin_Page</span> Instância do objeto.</p>
			</dd>
			<dd>
				<h4>setIcon($icon_url)</h4>
				<p>Adiciona um ícone para a página no menu.</p>
				<h5>Parâmetros</h5>
				<p><span class="name">$icon_url (string):</span> URL do ícone.</p>
				<h5>Retorno</h5>
				<p><span class="name">MR2WP_Admin_Page</span> Instância do objeto.</p>
			</dd>
			<dd>
				<h4>setPosition($position)</h4>
				<p>Insere o link do menu em uma posição específica.</p>
				<h5>Parâmetros</h5>
				<p><span class="name">$position (int):</span>Posição do item no menu, veja as posições:</p>
				<ul>
					<li>5 - below Posts</li>
					<li>10 - below Media</li>
					<li>15 - below Links</li>
					<li>20 - below Pages</li>
					<li>25 - below comments</li>
					<li>60 - below first separator</li>
					<li>65 - below Plugins</li>
					<li>70 - below Users</li>
					<li>75 - below Tools</li>
					<li>80 - below Settings</li>
					<li>100 - below second separator</li>
				</ul>
				<h5>Retorno</h5>
				<p><span class="name">MR2WP_Admin_Page</span> Instância do objeto.</p>
			</dd>
			<dd>
				<h4>addScript($name, $url)</h4>
				<p>Adiciona um script na página administrativa. Os scripts são incluídos com a função <a href="http://codex.wordpress.org/Function_Reference/wp_enqueue_script">wp_enqueue_script</a></p>
				<h5>Parâmetros</h5>
				<p><span class="name">$name (string):</span> Nome do script.</p>
				<p><span class="name">$url (string):</span> Opcional. URL do script, caso o script já esteja registrado este parâmetro é opcional.</p>
				<h5>Retorno</h5>
				<p><span class="name">MR2WP_Admin_Page</span> Instância do objeto.</p>
			</dd>
			<dd>
				<h4>addStyle($name, $url)</h4>
				<p>Adiciona uma folha de estilos CSS na página administrativa. Os stylesheets são incluídos com a função <a href="http://codex.wordpress.org/Function_Reference/wp_enqueue_style">wp_enqueue_style</a></p>
				<h5>Parâmetros</h5>
				<p><span class="name">$name (string):</span> Nome do stylesheet.</p>
				<p><span class="name">$url (string):</span> Opcional. URL do stylesheet, caso o stylesheet já esteja registrado este parâmetro é opcional.</p>
				<h5>Retorno</h5>
				<p><span class="name">MR2WP_Admin_Page</span> Instância do objeto.</p>
			</dd>
			<dd>
				<h4>setParent($menu_slug)</h4>
				<p>Adiciona a página como submenu de um menu existente.</p>
				<h5>Parâmetros</h5>
				<p><span class="name">$menu_slug (string):</span> Slug do menu onde este item será incluído.</p>
				<h5>Retorno</h5>
				<p><span class="name">MR2WP_Admin_Page</span> Instância do objeto.</p>
			</dd>
			<dd>
				<h4>addAction($callback)</h4>
				<p>Adiciona uma função de callback no hook da página.</p>
				<h5>Parâmetros</h5>
				<p><span class="name">$callback (string):</span>Função para ser executada.</p>
				<h5>Retorno</h5>
				<p><span class="name">MR2WP_Admin_Page</span> Instância do objeto.</p>
			</dd>
			<dd>
				<h4>getSlug()</h4>
				<p>Retorna o slug da página.</p>
				<h5>Retorno</h5>
				<p><span class="name">string</span> Slug da página.</p>
			</dd>
			<dd>
				<h4>save()</h4>
				<p>Salva a página.</p>
			</dd>
		</dl>

		<h2>MR2WP_Rewrite</h2>
		<p>A classe MR2WP_Rewrite facilita a reescrita de URLs.</p>
		<p>Veja esta funcionalidade nativa na <a href="http://codex.wordpress.org/Class_Reference/WP_Rewrite" target="_blank">documentação oficial</a>.</p>

		<dl>
			<dt>Métodos</dt>
			<dd>
				<h4>addRule($alias, $src)</h4>
				<p>Cria uma nova regra de reescrita de URL.</p>
				<h5>Parâmetros</h5>
				<p><span class="name">$alias (string):</span>Nova URL.</p>
				<p><span class="name">$src (string):</span>URL de destino.</p>
				<h5>Retorno</h5>
				<p><span class="name">MR2WP_Rewrite</span> Instância do objeto.</p>
			</dd>
			<dd>
				<h4>save()</h4>
				<p>Salva as regras criadas.</p>
			</dd>
		</dl>

		<h2>MR2WP_PostType</h2>
		<p>A Classe MR2WP_PostType facilita a manipulação de Post Types.</p>
		<p>Veja esta funcionalidade nativa na <a href="http://codex.wordpress.org/Class_Reference/WP_Rewrite" target="_blank">documentação oficial</a>.</p>

		<dl>
			<dt>Construtor</dt>
			<dd>
				<h4>MR2WP_PostType($name)</h4>
				<p>Cria uma nova instancia de MR2WP_PostType.</p>
				<h5>Parâmetros</h5>
				<p><span class="name">$name (string):</span> Nome do Post Type.</p>
			</dd>
			<dt>Métodos</dt>
			<dd>
				<h4>getName()</h4>
				<p>Retorna o nome do post type</p>
				<h5>Retorno</h5>
				<p><span class="name">MR2WP_PostType</span> Instância do objeto.</p>
			</dd>
			<dd>
				<h4>getLabel($label)</h4>
				<p>Retorna um label do post type</p>
				<h5>Parâmetros</h5>
				<p><span class="name">$label (string):</span> Label que deseja, sendo eles:</p>
				<ul>
					<li>name</li>
					<li>singular_name</li>
					<li>add_new</li>
					<li>add_new_item</li>
					<li>edit_item</li>
					<li>new_item</li>
					<li>all_items</li>
					<li>view_item</li>
					<li>search_items</li>
					<li>not_found</li>
					<li>not_found_in_trash</li>
					<li>parent_item_colon</li>
					<li>menu_name</li>
				</ul>
				<h5>Retorno</h5>
				<p><span class="name">MR2WP_PostType</span> Instância do objeto.</p>
			</dd>
			<dd>
				<h4>setLabels($labels)</h4>
				<p>Define os labels do post type.</p>
				<h5>Parâmetros</h5>
				<p><span class="name">$labels (array):</span>Labels do post type:</p>
				<ul>
					<li>name</li>
					<li>singular_name</li>
					<li>add_new</li>
					<li>add_new_item</li>
					<li>edit_item</li>
					<li>new_item</li>
					<li>all_items</li>
					<li>view_item</li>
					<li>search_items</li>
					<li>not_found</li>
					<li>not_found_in_trash</li>
					<li>parent_item_colon</li>
					<li>menu_name</li>
				</ul>
				<h5>Retorno</h5>
				<p><span class="name">MR2WP_PostType</span> Instância do objeto.</p>
			</dd>
			<dd>
				<h4>setArgs($args)</h4>
				<p>Define as propriedades do post type.</p>
				<h5>Parâmetros</h5>
				<p><span class="name">$args (array):</span> Propriedades do post type:</p>
				<ul>
					<li>public</li>
					<li>publicly_queryable</li>
					<li>show_ui</li>
					<li>show_in_menu</li>
					<li>show_in_nav_menus</li>
					<li>query_var</li>
					<li>menu_position</li>
					<li>capability_type</li>
					<li>hierarchical</li>
					<li>has_archive</li>
					<li>rewrite</li>
					<li>supports</li>
				</ul>
				<h5>Retorno</h5>
				<p><span class="name">MR2WP_PostType</span> Instância do objeto.</p>
			</dd>
			<dd>
				<h4>save()</h4>
				<p>Salva o post type.</p>
				<h5>Retorno</h5>
				<p><span class="name">Booelan</span> True se o post type foi salvo, False em caso de algum erro.</p>
			</dd>
		</dl>

		<h2>MR2WP_Taxonomy</h2>
		<p>A classe MR2WP_Taxonomy facilita a manipulação de Taxonomias.</p>
		<p>Veja esta funcionalidade nativa na <a href="http://codex.wordpress.org/Function_Reference/register_taxonomy" target="_blank">documentação oficial</a>.</p>

		<dl>
			<dt>Construtor</dt>
			<dd>
				<h4>MR2WP_Taxonomy($name)</h4>
				<p>Cria uma nova instancia de MR2WP_Taxonomy.</p>
				<h5>Parâmetros</h5>
				<p><span class="name">$name (string):</span> Nome da taxonomia.</p>
			</dd>
			<dt>Métodos</dt>
			<dd>
				<h4>setArgs( $args )</h4>
				<p>Define as propriedades da taxonomia.</p>
				<h5>Parâmetros</h5>
				<p><span class="name">$args (string):</span> Lista de argumentos da taxonomia</p>
				<ul>
					<li>label</li>
					<li>hierarchical</li>
					<li>show_ui</li>
					<li>query_var</li>
					<li>show_in_nav_menus</li>
					<li>public</li>
				</ul>
				<h5>Retorno</h5>
				<p><span class="name">MR2WP_Taxonomy</span> Instância do objeto.</p>
			</dd>
			<dd>
				<h4>showInColumn()</h4>
				<p>Mostra os termos em uma coluna na listagem de posts.</p>
				<h5>Retorno</h5>
				<p><span class="name">MR2WP_Taxonomy</span> Instância do objeto.</p>
			</dd>
			<dd>
				<h4>showInFilter()</h4>
				<p>Adiciona um filtro de posts por termo na listagem de posts.</p>
				<h5>Retorno</h5>
				<p><span class="name">MR2WP_Taxonomy</span> Instância do objeto.</p>
			</dd>
			<dd>
				<h4>setPostType( $posttype )</h4>
				<p>Define o post type para aplicar a taxonomia.</p>
				<h5>Parâmetros</h5>
				<p><span class="name">$posttype (MR2WP_PostType):</span> Instância de MR2WP_PostType do post type.</p>
				<h5>Retorno</h5>
				<p><span class="name">MR2WP_Taxonomy</span> Instância do objeto.</p>
			</dd>
			<dd>
				<h4>save()</h4>
				<p>Salva a taxonomia.</p>
			</dd>
		</dl>

		<h2>MR2WP_MetaBox</h2>
		<p>A classe MR2WP_MetaBox facilita a manipulação de post meta boxes.</p>
		<p>Veja esta funcionalidade nativa na <a href="http://codex.wordpress.org/Function_Reference/add_meta_box" target="_blank">documentação oficial</a>.</p>

		<dl>
			<dt>Construtor</dt>
			<dd>
				<h4>MR2WP_MetaBox($id, $title, $priority, $context )</h4>
				<p>Cria uma nova instancia de MR2WP_MetaBox.</p>
				<h5>Parâmetros</h5>
				<p><span class="name">$id (string):</span> Identificador do meta box.</p>
				<p><span class="name">$title (string):</span> Título do meta box. Default: "Opções".</p>
				<p><span class="name">$priority (string):</span> Prioridade do meta box. Default: "default".</p>
				<p><span class="name">$context (string):</span> Contexto do meta box. Default: "advanced".</p>
			</dd>
			<dt>Métodos</dt>
			<dd>
				<h4>getID()</h4>
				<p>Retorna o id do meta box</p>
				<h5>Retorno</h5>
				<p><span class="name">string</span> Id do meta box</p>
			</dd>
			<dd>
				<h4>getMetas()</h4>
				<p>Retorna todos os post meta adicionados ao meta box</p>
				<h5>Retorno</h5>
				<p><span class="name">array</span> Objetos filhos de MR2WP_Meta</p>
			</dd>
			<dd>
				<h4>getPostType()</h4>
				<p>Retorna o post type a que pertence</p>
				<h5>Retorno</h5>
				<p><span class="name">MR2WP_PostType</span> objeto do post type.</p>
			</dd>
			<dd>
				<h4>setPostType( $posttype )</h4>
				<p>Define o post type onde o meta box deve aparecer</p>
				<h5>Parâmetros</h5>
				<p><span class="name">$posttype (MR2WP_PostType)</span> Instância do post type.</p>
				<h5>Retorno</h5>
				<p><span class="name">MR2WP_MetaBox</span> Instância do objeto.</p>
			</dd>
			<dd>
				<h4>addMeta($meta)</h4>
				<p>Adiciona um post meta ao meta box</p>
				<h5>Parâmetros</h5>
				<p><span class="name">$meta (MR2WP_Meta)</span> Instância do post meta.</p>
				<h5>Retorno</h5>
				<p><span class="name">MR2WP_MetaBox</span> Instância do objeto.</p>
			</dd>
			<dd>
				<h4>setTemplate($template)</h4>
				<p>Define o template do meta box</p>
				<h5>Parâmetros</h5>
				<p><span class="name">$template (string)</span> Nome do arquivlo .php com o template. O arquivo deve estar na pasta de templates: MR2WP/app/core/Templates.</p>
				<h5>Retorno</h5>
				<p><span class="name">MR2WP_MetaBox</span> Instância do objeto.</p>			
			</dd>
			<dd>
				<h4>save()</h4>
				<p>Salva o meta box.</p>
			</dd>
		</dl>

		<h2>MR2WP_Meta</h2>
		<p>A classe MR2WP_Meta é uma classe abstrata, por isso não pode ser instanciada diretamente. Isso por que não representa nenhum componente visual.</p>
		<p>Para utilizar os recursos dessa classe é preciso que outra classe herde suas funcionalidades.</p>
		<dl>
			<dt>Construtor</dt>
			<dd>
				<h4>MR2WP_Meta($name, $label, $attrs, $options)</h4>
				<p>Cria uma instância de MR2WP_Meta, apesar de não poder ser intânciados, as intâncias das classes filhas de MR2WP_Meta executam o construtor da classe pai.</p>
				<p>O objetivo de alguns atributos e métodos dessa classe podem variar de acordo com o seu contexto. Isso fica mais claro em atributos como a $options, que em um <?php echo htmlentities("<select>"); ?> podem ser as <?php echo htmlentities("<options>"); ?>, já em controle que ulize um plugin jquery podem ser as opções de configuração do plugin.</p>
				<h5>Parâmetros</h5>
				<p><span class="name">$name (string)</span> Nome da meta (meta key).</p>
				<p><span class="name">$label (string)</span> Label da meta.</p>
				<p><span class="name">$attrs (array)</span> Array com atributos html. Default: array()</p>
				<p><span class="name">$options (array)</span> Array com opções da meta. Default: array()</p>
			</dd>
			<dt>Métodos</dt>
			<dd>
				<h4>getName()</h4>
				<p>Retorna o nome (meta key) do post meta.</p>
				<h5>Retorno</h5>
				<p><span class="name">string</span> Nome (meta key) do post meta.</p>
			</dd>
			<dd>
				<h4>getLabel()</h4>
				<p>Retorna o label do post meta.</p>
				<h5>Retorno</h5>
				<p><span class="name">string</span> Label do post meta.</p>
			</dd>
			<dd>
				<h4>getAttrs()</h4>
				<p>Retorna todos os atributos do post meta</p>
				<h5>Retorno</h5>
				<p><span class="array">array</span> Array associativo dos atributos.</p>
			</dd>
			<dd>
				<h4>getAttr($name, $defautl)</h4>
				<p>Retorna um atributo</p>
				<h5>Atributos</h5>
				<p><span class="name">$name</span> Nome do atributo.</p>
				<p><span class="name">$default (opcional)</span> Valor de retorno caso o atributo não exista. Default: null</p>
				<h5>Retorno</h5>
				<p><span class="name">mixed </span> Valor do atributo.</p>
			</dd>
			<dd>
				<h4>getValue()</h4>
				<p>Retorna o atributo value do post meta.</p>
				<h5>Retorno</h5>
				<p><span class="name">mixed </span> Valor da meta.</p>
			</dd>
			<dd>
				<h4>addOption($option, $value)</h4>
				<p>Adiciona uma opção</p>
				<h5>Parâmetros</h5>
				<p><span class="name">$option (string)</span> Nome da opção.</p>
				<p><span class="name">$value (midex)</span> Valor da opção</p>
				<h5>Retorno</h5>
				<p><span class="name">MR2WP_Meta</span> Instância do objeto.</p>
			</dd>
			<dd>
				<h4>addAttr($name, $value)</h4>
				<p>Adiciona um atributo.</p>
				<h5>Parâmetros</h5>
				<p><span class="name">$name (string)</span> Nome do atributo.</p>
				<p><span class="name">$value (mixed)</span> Valor do atributo.</p>
				<h5>Retorno</h5>
				<p><span class="name">MR2WP_Meta</span> Instância do objeto.</p>
			</dd>
			<dd>
				<h4>getOption($option, $default)</h4>
				<p>Retorna uma opção</p>
				<h5>Parâmetros</h5>
				<p><span class="name">$option (string)</span> Nome da opção.</p>
				<p><span class="name">$default (opcional)</span> Valor de retorno caso a opção não exista. Default: null</p>
				<h5>Retorno</h5>
				<p><span class="name">mixed</span> Valor da opção</p>
			</dd>
			<dd>
				<h4>getOptions()</h4>
				<p>Retorna todas as opções do post meta</p>
				<h5>Retorno</h5>
				<p><span class="array">array</span> Array associativo das opções.</p>
			</dd>
		</dl>
	</div>
<?php get_footer(); ?>