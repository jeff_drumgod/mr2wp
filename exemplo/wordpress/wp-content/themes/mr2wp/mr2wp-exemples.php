<?php
	error_reporting(E_NONE);
	
	include dirname(__FILE__) . "/MR2WP/MR2WP.php";

	/* MR2WP_User_Role
	---------------------------------------------------------- */

	$client = new MR2WP_User_Role("cliente", "Cliente");
	$client->removeField("aim");

	/* MR2WP_User_Role [end]
	---------------------------------------------------------- */

	/* MR2WP_User_MetaBox
	---------------------------------------------------------- */

	$user_metabox = new MR2WP_User_MetaBox("cobranca", "Endereço de cobrança");
	$user_metabox
		->setRole($client)
		->addMeta(new MR2WP_Meta_Input("estado", "Estado"))
		->addMeta(new MR2WP_Meta_Input("cidade", "Cidade"))
		->addMeta(new MR2WP_Meta_Input("endereco", "Endereço"))
		->addMeta(new MR2WP_Meta_Input("bairro", "Bairro"))
		->save();

	$user_metabox = new MR2WP_User_MetaBox("entrega", "Endereço de entrega");
	$user_metabox
		->setRole($client)
		->addMeta(new MR2WP_Meta_Input("entrega_estado", "Estado"))
		->addMeta(new MR2WP_Meta_Input("entrega_cidade", "Cidade"))
		->addMeta(new MR2WP_Meta_Input("entrega_endereco", "Endereço"))
		->addMeta(new MR2WP_Meta_Input("entrega_bairro", "Bairro"))
		->save();

	/* MR2WP_User_MetaBox [end]
	---------------------------------------------------------- */

	/* MR2WP_Admin_Page
	---------------------------------------------------------- */

	# Admin page com template físico
	$admin_page_lojas = new MR2WP_Admin_Page("Lojas", "Lojas");
	$admin_page_lojas
		->setCapability("edit_posts")
		->setTemplate(dirname(__FILE__) . "/inc/admin-page-lojas-tpl.php")
		->setIcon(get_bloginfo( "template_url" ) . "/assets/img/shop_cart.png")
		->setPosition(5)
		->addScript("admin-lojas-js", get_bloginfo( "template_url" ) . "/assets/js/admin-lojas.js")
		->addStyle("admin-lojas-css", get_bloginfo( "template_url" ) . "/assets/css/admin-lojas.css")
		->save();

	# Admin page com template via callback
	$admin_page_franquias = new MR2WP_Admin_Page("Franquias", "Franquias");
	$admin_page_franquias
		->setCapability("edit_posts")
		->setCallback_function("callback_admin_page")
		->setParent($admin_page_lojas->getSlug())
		->save();

	function callback_admin_page(){
		echo "<h3>Franquias</h3>";
		echo "<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur, adipisci, illum fugit deleniti beatae eligendi est iure obcaecati temporibus repellendus praesentium dolore dicta sunt quasi earum eveniet soluta facilis. Vero?</p>";
	}

	/* MR2WP_Admin_Page [end]
	---------------------------------------------------------- */

	/* MR2WP_Rewrite
	---------------------------------------------------------- */
	$rewrite = new MR2WP_Rewrite();
	$rewrite
		->addRule("images/(.*)(.png|.jpg|.jpeg|.gif)", "wp-content/themes/mr2wp/assets/img/$1$2")
		->addRule("css/(.*)(.css)", "wp-content/themes/mr2wp/assets/css/$1$2")
		->addRule("img/(.*)", "wp-content/themes/mr2wp/assets/img/$1")
		->save();

	/* MR2WP_Rewrite [end]
	---------------------------------------------------------- */

	/* MR2WP_PostType
	---------------------------------------------------------- */
	$product = new MR2WP_PostType("product");
	$product
		->setLabels(array(
			"name" => "Produtos",
			"singular_name" => "Produto",
			"menu_name" => "Produtos"
		))
		->setArgs(array(
			"menu_position" => 6,
			"rewrite" => array( "slug" => "produtos" ),
			"supports" => array( "title", "thumbnail", "tags" )
		))
		->save();

	/* MR2WP_PostType [end]
	---------------------------------------------------------- */

	/* MR2WP_Taxonomy
	---------------------------------------------------------- */
	$department = new MR2WP_Taxonomy("department");
	$department
		->setArgs(array(
			"label" => "Departamentos"
		))
		->setPostType($product)
		->showInColumn()
		->showInFilter()
		->save();
	/* MR2WP_Taxonomy [end]
	---------------------------------------------------------- */

	/* MR2WP_MetaBox
	---------------------------------------------------------- */
	$metabox = new MR2WP_MetaBox("metabox_test");
	$metabox
		->setPostType($product)
		->setTemplate("MetaBoxClean.php")
		->save();

	$metabox = new MR2WP_MetaBox("metas_test");
	$metabox->setPostType($product);
	/* MR2WP_MetaBox [end]
	---------------------------------------------------------- */

	/* MR2WP_Meta
	---------------------------------------------------------- */

	# MR2WP_Meta_Input
	$peso = new MR2WP_Meta_Input("peso", "Peso");
	$metabox->addMeta($peso);

	# MR2WP_Meta_CheckboxGroup
	$cores = new MR2WP_Meta_CheckboxGroup("cores", "Cores disponíveis");
	$cores
		->addOption(new MR2WP_Meta_Input("azul", "Azul", array("type" => "checkbox")))
		->addOption(new MR2WP_Meta_Input("amarelo", "Amarelo", array("type" => "checkbox")))
		->addOption(new MR2WP_Meta_Input("vermelho", "Vermelho", array("type" => "checkbox")));
	$metabox->addMeta($cores);

	# MR2WP_Meta_RadioGroup
	$voltagem = new MR2WP_Meta_RadioGroup("voltagem", "Voltagem");
	$voltagem
		->addAttr("title", "Qual a voltagem do aparelho?")
		->addOption(new MR2WP_Meta_Input("110", "110V"))
		->addOption(new MR2WP_Meta_Input("220", "220V"))
		->addOption(new MR2WP_Meta_Input("bivolt", "110V / 220V"));
	$metabox->addMeta($voltagem);

	# MR2WP_Meta_Select
	$ativo = new MR2WP_Meta_Select("ativo", "Ativo");
	$ativo
		->addOption(new MR2WP_Meta_Option("", ""))
		->addOption(new MR2WP_Meta_Option("sim", "Sim"))
		->addOption(new MR2WP_Meta_Option("nao", "Não"));
	$metabox->addMeta($ativo);

	# MR2WP_Meta_Textarea
	$descricao = new MR2WP_Meta_Textarea("descricao", "Descrição");
	$metabox->addMeta($descricao);

	# MR2WP_Meta_Editor 
	$descricao_longa = new MR2WP_Meta_Editor("descricao_longa", "Descrição Longa");
	$metabox->addMeta($descricao_longa);

	# MR2WP_Meta_Date
	$fabricacao = new MR2WP_Meta_Date("fabricacao", "Fabricação");
	$metabox->addMeta($fabricacao);

	# MR2WP_Meta_File
	$especificacoes = new MR2WP_Meta_File("especificacoes", "Especificações técnicas");
	$metabox->addMeta($especificacoes);

	# MR2WP_Meta_Image
	$imagem = new MR2WP_Meta_Image("imagem", "Imagem grande");
	$metabox->addMeta($imagem);

	# Reapetable
	$repeatable = new MR2WP_Meta_Repeatable("dimensões", "Dimensões");
	$repeatable->setControll(new MR2WP_Meta_Input("altura", "Altura"));
	$metabox->addMeta($repeatable);

	# Repeatable Multiple
	$multiple = new MR2WP_Meta_RepeatableMultiple("qualidade", "Testes de qualidade");
	$multiple
		->addControll(new MR2WP_Meta_Date("data", "Data"))
		->addControll(new MR2WP_Meta_Input("nome", "Nome"))
		->addControll(new MR2WP_Meta_Input("resultado", "Resultado"));
	$metabox->addMeta($multiple);

	$nota = new MR2WP_Meta_Slider("nota", "Nota");
	$nota->addOption("max", 10);
	$metabox->addMeta($nota);

	$localizacao = new MR2WP_Meta_GoogleMaps("localizacao", "Localização");
	$metabox->addMeta($localizacao);
	
	$metabox->save();

	#Video Gallery
	$videos = new MR2WP_MetaBox("galeria_videos", "Galeria de vídeos");
	$videos
		->setTemplate("MetaBoxClean.php")
		->addMeta(new MR2WP_Meta_VideoGallery("videos", "Adicione aqui os vídeos para a galeria."))
		->setPostType($product)
		->save();
	/* MR2WP_Meta [end]
	---------------------------------------------------------- */