<?php

include dirname(__FILE__) . "/mr2wp-exemples.php";

define('SITE_URL', get_bloginfo('wpurl'));
define('THEME_URL', get_bloginfo('template_url'));
define('THEME_DIR', dirname(__FILE__));

wp_register_style( "home-css", sprintf("%s/assets/css/home.css", THEME_URL) );

add_action("wp_enqueue_scripts", "mr2wp_theme_wp_head");

function mr2wp_theme_wp_head(){
	if(is_home()){
		wp_enqueue_style( "home-css" );
	}
}

add_filter('show_admin_bar', 'mr2wp_show_admin_bar');
function mr2wp_show_admin_bar(){
	return false;
}

?>