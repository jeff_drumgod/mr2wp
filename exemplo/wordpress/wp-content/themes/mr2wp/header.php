<!doctype html>
<html lang="pt_BR">
<head>
	<meta charset="UTF-8">
	<title>MR2WP</title>

	<link rel="stylesheet" href="<?php echo SITE_URL; ?>/css/documentacao.css">
</head>

<body>
	<header id="header">
		<div class="wrap">
			<h1>MR2WP</h1>
			<nav class="nav">
				<ul class="menu">
					<li class="item">
						<a href="#" class="link">Link</a>
						<ul class="sub">
							<li class="item"><a href="#" class="link">Link</a></li>
							<li class="item"><a href="#" class="link">Link</a></li>
							<li class="item"><a href="#" class="link">Link</a></li>
						</ul>
					</li>
					<li class="item"><a href="#" class="link">Link</a></li>
					<li class="item"><a href="#" class="link">Link</a></li>
					<li class="item"><a href="#" class="link">Link</a></li>
					<li class="item"><a href="#" class="link">Link</a></li>
				</ul>
			</nav>
		</div>
	</header>

	<section id="main">