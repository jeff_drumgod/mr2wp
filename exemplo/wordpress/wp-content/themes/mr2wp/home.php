<?php get_header(); ?>

<div class="nav">
	<ul class="menu">
		<li class="item"><a href="/documentacao" class="link">documentação</a></li>
		<li class="item"><span class="div">/</span></li>
		<li class="item"><a href="#" class="link">instalação</a></li>
		<li class="item"><span class="div">/</span></li>
		<li class="item"><a href="#" class="link">bitbucket</a></li>
		<li class="item"><span class="div">/</span></li>
		<li class="item"><a href="#" class="link">download</a></li>
	</ul>
</div>

<div class="text">
	<h1 class="title">MR2WP</h1>
	<p class="subtitle">Orientado a objetos MVC framework para wordpress</p>
</div>

<?php get_footer(); ?>