<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mr2wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'V1%W0VK(_4W2I{$j3 GX(ejXRdpcy{qaAj:g%N$SjX:ej|i&S!0VWhI)[9im/>(6');
define('SECURE_AUTH_KEY',  'I@vn[Cp+3_BGVZv@EG2-kK%X8}$A%VVHN+iz_<j]@&&|Cx;h_Z>Kt?>M!!6R<;Rk');
define('LOGGED_IN_KEY',    'h-3bMsg;0o%{q+Iq]Pr?%:n=TMj?[P|rZ1k]H.6SgXoF]M)tBf[;4Y>R`s0.4)W0');
define('NONCE_KEY',        '(q`/hYr|Bw.Dkh{zI_={U?AHX%P6w%gH6z+$@j?U#@XfJf7|}1PWaI#^`)~CYz#>');
define('AUTH_SALT',        'x;) )AP.A-Lj:Bs`fR|83o[yNw!aRjoPfsg2VtWQy{;w[//?eHDd=~avC/Kea[bn');
define('SECURE_AUTH_SALT', ']_pNJ!A-%yt(xVNCO4$-.0<@,Aa7W15f5J_5Gq,B_1a9S7xpa*D]m##xRVS=yTuc');
define('LOGGED_IN_SALT',   'aK?D|/3 EclWM;G7oD#u7]F@H90ce|m2D!9x{j&Hp%-S!0|$DG3dX.B}>|xqiK`H');
define('NONCE_SALT',       'WTs>^fC8yA_`qmk{mv0g!Qc$!!#i.XqCp.1Qp^rm=ARUcK}qDoC_e+L,%GPyI=Su');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
