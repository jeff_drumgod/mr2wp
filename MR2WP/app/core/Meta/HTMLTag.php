<?php
class MR2WP_Meta_HTMLTag {
	
	private $_args;
	private $_id;
	private $_tag;
	private $_content;
	private $_template;
	
	/**
	 * Embed html elemment to metabox.
	 * @param string $id
	 * @param strin $tag - ex.: "p", "a", "div", "h2"
	 * @param string $content
	 * @param array:string=>string $args - ex: "href" => "#"
	 */
	public function __construct( $id, $tag, $content = "", $args = array() ){
		
		$this->_id = $id;
		$this->_tag = $tag;
		$this->_content = $content;
		$this->_args = $args;
		
		$this->setTemplate("HTMLTag.php");
		
	}
	
	/**
	 * set a content
	 * @param string $content
	 */
	public function setContent( $content ){
		
		$this->_content = $content;
		
		return $this;
	}
	
	/**
	 * set a template
	 * @param string $template - template file name 
	 */
	public function setTemplate( $template ){

		$this->_template = $this->_template = dirname(__FILE__) . "/../Templates/Meta/{$template}";
		
	}
	
	/**
	 * return id 
	 * @return string
	 */
	public function getName(){
		return $this->_id;
	}
	
	/**
	 * Bind the HTML for component
	 * @throws ErrorException - if template not exist
	 * @return string - html
	 */
	public function display(){
		
		if(file_exists($this->_template)){
			
			return $this->render($viewData);
			
		} else {
			
			throw new ErrorException("Template $this->_template not found.");
			
		}
		
	}
	
	/**
	 * -------------------------------------
	 * Render meta
	 * -------------------------------------
	 *
	 * @param null $viewData - any data to be used within the template.
	 * @return string -
	 *
	 */
	public function render( $viewData = null ) {

		( $viewData ) ? extract( $viewData ) : null;
		ob_start();
		require ( $this->_template );
		$template = ob_get_contents();
		ob_end_clean();
		return $template;
	}
	
}