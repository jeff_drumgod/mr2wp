<?php
class MR2WP_Meta_Image extends MR2WP_Meta {

	public function __construct($name, $label, $attrs = array()){

		$this->_template = "Image.php";
		$this->addHeadCallBack(array(&$this, 'adminHead'));

		parent::__construct($name, $label, $attrs);

	}

	/**
	 * Call back acionado pela action admin_head
	 */
	public function adminHead(){
		?>
		<script type="text/javascript">
		jQuery(document).ready(function(){

			jQuery('.custom_upload_image_button').click(function() {
				formfield = jQuery(this).siblings('.custom_upload_image');
				preview = jQuery(this).siblings('.custom_preview_image');
				tb_show('', 'media-upload.php?type=image&TB_iframe=true');
				window.send_to_editor = function(html) {
					imgurl = jQuery('<div></div>').html(html).find('img').attr("src");
					id = jQuery(html).text();
					formfield.val(imgurl);
					preview.attr('src', imgurl);
					tb_remove();
				}

				return false;
			});

			jQuery('.custom_clear_image_button').click(function() {
				var defaultImage = jQuery(this).parent().siblings('.custom_default_image').text();
				jQuery(this).parent().siblings('.custom_upload_image').val('');
				jQuery(this).parent().siblings('.custom_preview_image').attr('src', defaultImage);
				return false;
			});

		});
		</script>
		<?php
	}

}