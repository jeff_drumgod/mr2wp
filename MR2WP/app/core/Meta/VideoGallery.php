<?php
/**
 * MR2WP_Meta_VideoGallery - Post meta para galeria de vídeos.
 * @author Marcel Rodrigo
 * @version 2.0
 * @date 2013-12-28
 * @category Meta
 * @link http://mr2wp.marcelrodrigo.com.br
 * @package MR2WP
 *
 */
class MR2WP_Meta_VideoGallery extends MR2WP_Meta_Multimidia {

	public function __construct($name, $label){

		$this->_template = "VideoGallery.php";
		$this->addHeadCallBack(array(&$this, 'adminHead'));

		parent::__construct($name, $label, array());

	}

	public function adminHead(){
		add_thickbox();
		?>

		<script type="text/javascript">
			jQuery(document).ready(function(){
				jQuery(function($) {

					/** declare
					----------------------------- */
					var $container = $("#<?php echo $this->getName(); ?>-container"),
						$modal_container = $("#<?php echo $this->getName(); ?>-modal-container"),
						$search_input = $modal_container.find(".search-bar .input"),
						$video_list = $container.find(".videos");
						video_info = null;

					/** bindings
					----------------------------- */
					$modal_container.find(".search-bar .button").on("click", searchBar_buttonAdd_onClick);
					$modal_container.find(".actions .selecionar").on("click", modalContainer_buttonSelecionar_onClick);
					$modal_container.find(".actions .cancelar").on("click", modalContainer_cancelar_onClick);

					/** event callbacks
					----------------------------- */

					/**
					* evento de clique do botão de procurar video no modal
					*/
					function searchBar_buttonAdd_onClick(event){
						if($search_input.val() != ""){
							jQuery.ajax({
								url: "<?php echo MR2WP_URL; ?>/app/ajax/index.php",
								dataType: "json",
								data: "action=get_video&video_url=" + $search_input.val(),
								success: function( data ){
									if(data.result){

										$modal_container.find(".thumb").attr("src", data.video_info.thumbs.mq);
										$modal_container.find(".video-title").val(data.video_info.title);
										$modal_container.find(".video-description").val(data.video_info.content);
										$modal_container.find(".video-info").fadeIn();

										$modal_container.find(".actions").fadeIn();

									} else {
										alert("Vídeo não encontrado.");
									}
								}
							});

						} else {
							alert("Informe a URL do vídeo.");
						}
					}

					function modalContainer_buttonSelecionar_onClick(event){
						jQuery.ajax({
							url: "<?php echo MR2WP_URL; ?>/app/ajax/index.php?action=video_gallery-insert",
							type: "post",
							dataType: "json",
							data: {
								url: $search_input.val(), 
								title: $modal_container.find(".video-title").val(), 
								description: $modal_container.find(".video-title").val(),
								author_id: $("#post_author").val(),
								post_type: "<?php echo $this->_defaults['post_type'] ?>",
								taxonomy: "<?php echo $this->_defaults['tax_name'] ?>",
								meta: "<?php echo $this->getName() ?>"
							},
							success: function( data ){
								if(data.result){
									modalContainer_cancelar_onClick(null);
									addVideo(data);
								} else {
									alert("Erro ao salvar vídeo: " + data.errors.join());
								}
							}
						});		
					}

					function modalContainer_cancelar_onClick(event){
						console.log(1);
						tb_remove();
						video_info = null;
						modalContainer_clearPreview();
					}

					/** functions
					----------------------------- */

					/**
					* Limpa o vídeo encontrado do modal
					*/
					function modalContainer_clearPreview(){
						$modal_container.find(".thumb").attr("src", null);
						$modal_container.find(".video-title").val(null);
						$modal_container.find(".video-description").val(null);

						$modal_container.find(".video-info").fadeOut();
						$modal_container.find(".actions").fadeOut();
					}

					/**
					* Adiciona um vídeo na listagem
					*/
					function addVideo(response_data){
						var $li = $("<li></li>", {
							id: "post-" + response_data.post_id,
							class: "video"
						});

						$li.append($("<img/>", {
							src: response_data.thumb_url,
							title: response_data.title,
							alt: response_data.title
						})).append($("<a></a>"), {
							href: "#",
							html: "Remover vídeo",
							title: "Remover	vídeo",
							class: "remove"
						});
					}

				});    
			});
		</script>

		<?php
	}
	
}