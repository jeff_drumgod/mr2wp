<?php
class MR2WP_Meta_Textarea extends MR2WP_Meta {
	
	public function __construct($name, $label, $attrs = array()){
		
		$this->_template = "Textarea.php";
		
		parent::__construct($name, $label, $attrs);
		
	}
	
}