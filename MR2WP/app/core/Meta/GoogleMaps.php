<?php
class MR2WP_Meta_GoogleMaps extends MR2WP_Meta {

	public function __construct($name, $label){

		$this->_template = "GoogleMaps.php";
		$this->addHeadCallBack(array(&$this, 'adminHead'));

		parent::__construct($name, $label);

	}

	public function adminHead(){

		wp_enqueue_script("maps", "https://maps.googleapis.com/maps/api/js?key=AIzaSyAUZJj6j7rVZnmKst4imnlPMgCEGXNiMeA&sensor=true");

		?>

		<script type="text/javascript">

			jQuery(document).ready(function(){
	    	  	initialize();

				jQuery("#<?php echo $this->getName(); ?>-container button").bind(
					"click",
					function( event ){
						event.preventDefault();

						codeAddress();
					}
		    	  );

				var geocoder;
			  	var map;
			  	var beachMarker;

			  	function makeMap(lat, lng) {

			  		geocoder = new google.maps.Geocoder();

			  		var mapOptions = {
				  		center: new google.maps.LatLng(lat, lng),
				  		zoom: 15,
				  		mapTypeId: google.maps.MapTypeId.ROADMAP
			  		};

			  		map = new google.maps.Map(
					  	document.getElementById("<?php echo $this->getName(); ?>-map"),
			  			mapOptions
			  		);

					var image = '<?php echo MR2WP_URL; ?>/static/img/map-pin.png';

					beachMarker = new google.maps.Marker({
						position: mapOptions.center,
						map: map,
						icon: image,
						draggable: true
					});

					google.maps.event.addListener(beachMarker, 'dragend', marker_dragend );

			  	}

				function marker_dragend(marker){

					jQuery("#<?php echo $this->getName(); ?>").val(marker.latLng.lat() + "," + marker.latLng.lng());

				}

			  	function initialize() {

					var str = jQuery("#<?php echo $this->getName(); ?>").val();

					if(str && str != undefined)
					{
						var value = str.split(",");

						makeMap(value[0], value[1]);
					} else if (navigator.geolocation) {
			  	        navigator.geolocation.getCurrentPosition(function(position) {
			  	            makeMap(position.coords.latitude, position.coords.longitude);
			  	          	jQuery("#<?php echo $this->getName(); ?>").val(position.coords.latitude + "," + position.coords.longitude);
			  	        });
			  	    } else {
			  	        makeMap(-25.4368967, -49.2339513);
			  	      	jQuery("#<?php echo $this->getName(); ?>").val(-25.4368967 + "," + -49.2339513);

			  	    }

				}

			  	function codeAddress() {
			  		var address = document.getElementById("<?php echo $this->getName(); ?>-finder").value;

			  		geocoder.geocode( { 'address': address}, function(results, status) {
			  			if (status == google.maps.GeocoderStatus.OK) {
				  		  	map.setCenter(results[0].geometry.location);
				  		  	beachMarker.setPosition(map.center);
				  		  	jQuery("#<?php echo $this->getName(); ?>").val(beachMarker.position.lb + "," + beachMarker.position.mb);
				  		  } else {
				  		  	alert("Geocode was not successful for the following reason: " + status);
				  		  }
			  		});
			  	}

			});
	    </script>

		<?php

	}
}