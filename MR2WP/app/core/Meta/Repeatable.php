<?php
class MR2WP_Meta_Repeatable extends MR2WP_Meta {
	
	private $_controll;
	
	public function __construct($name, $label, $attrs = array()){
		
		$this->_template = "Repeatable.php";
		$this->addHeadCallBack(array(&$this, 'adminHead'));
		
		parent::__construct($name, $label, $attrs);
		
	}
	
	/**
	 * Seta o controle que será utilizado para repetição. Poderá ser um objeto extendido de MR2WP_Meta, ou um array de objetos MR2WP_Meta
	 * @param mixed $controll
	 */
	public function setControll( MR2WP_Meta $controll ){

		$this->_controll = $controll;
	}
	
	/**
	 * Retorna o controle. Deverá ser um objeto extendido de MR2WP_Meta
	 * @return mixed
	 */
	public function getControll(){		
		return $this->_controll;
	}
	
	/**
	 * Call back acionado pela action admin_head
	 */
	public function adminHead(){
		
		?>
			<script type="text/javascript">
				jQuery(document).ready(function(){


					jQuery('.repeatable-add[rel=<?php echo $this->getName(); ?>-repeatable]').on("click", function(event) {
						
						event.preventDefault();
						var $closest = jQuery(this).closest('td');
						var $fieldLocation = $closest.find('ul li:last');
						
						if($fieldLocation.css('display') == 'none'){
						
							$fieldLocation.show();
						
						} else {
							
							var $field = $fieldLocation.clone(true, true);
														
							// jQuery('input', $field).attr('name', function(index, name) {
							// 	return name.replace(/(\d+)/, function(fullMatch, n) {
							// 		return Number(n) + 1;
							// 	});
							// });

							jQuery('input:not([type=button])', $field).removeAttr("value");

							$field.insertAfter($fieldLocation, $closest);
							//fixNames($closest);

							$closest.find(".hasDatepicker").each(function(i){
								var this_id = jQuery(this).attr("id"); // current inputs id
								var new_id = this_id +i; // a new id
								jQuery(this).attr("id", new_id); // change to new id
								jQuery(this).removeClass('hasDatepicker'); // remove hasDatepicker class
								jQuery(this).datepicker(); // re-init datepicker
							});
							
						}
						
					});
					
					jQuery('.repeatable-remove').live('click', function(){
						var $parent = jQuery(this).parent();
						var $closest = jQuery(this).closest('td');
						if ($parent.siblings().length > 0) {
							jQuery(this).parent().remove();
						} else {
							jQuery(this).parent().hide();
							jQuery(this).parent().find('input').val('');
						}
						fixNames($closest);
						return false;
					});
				});

				function fixNames ($closest) {
					$closest.find('.custom_repeatable li').each(function(i){
						jQuery('input', jQuery(this)).attr('name', function (index, name){
							return name.replace(/(\d+)/, function(fullMatch, n) {
								return i;
							});
						});
					});
				}
			</script>
			<?php
		}
	
}