<?php
class MR2WP_Meta_Color extends MR2WP_Meta_Input {

	public function __construct($name, $label, $attrs = array()){

		$this->addHeadCallBack(array(&$this, 'adminHead'));
		parent::__construct($name, $label, $attrs);

	}

	/**
	 * Call back acionado pela action admin_head
	 */
	public function adminHead(){

		wp_enqueue_script('iris');

		?>
			<script type="text/javascript">
				jQuery(document).ready(function(){
					jQuery( "#<?php echo $this->getName(); ?>" ).iris();
				});
			</script>
			<?php
		}


}