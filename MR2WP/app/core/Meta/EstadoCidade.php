<?php
class MR2WP_Meta_EstadoCidade extends MR2WP_Meta {
	
	public function __construct($name, $label, $attrs = array()){
		
		$this->_template = "EstadoCidade.php";
		$this->addHeadCallBack(array(&$this, 'adminHead'));
		
		parent::__construct($name, $label, $attrs);
		
	}
	
	public function adminHead(){
	
		wp_enqueue_script("cidades-estados", "http://cidades-estados-js.googlecode.com/files/cidades-estados-1.2-utf8.js");
		
		?>

			<script type="text/javascript">
			 	jQuery(document).ready(function(){

			 		var $estado = jQuery("#<?php echo $this->getName() ?>-estado");
			 		var $cidade = jQuery("#<?php echo $this->getName() ?>-cidade");
			 		
					var options = {
						cidade: $cidade.get(0),
			          	estado: $estado.get(0),
			          	estadoVal: $estado.attr("data-value"),
			          	cidadeVal: $cidade.attr("data-value")
		          	
					};
					
			 		new dgCidadesEstados(options);
			 	});
			</script>
			
		<?php
	
	}
	
	
}