<?php
class MR2WP_Meta_Editor extends MR2WP_Meta {
	
	public function __construct($name, $label, $attrs = array()){
		
		parent::__construct($name, $label, $attrs);
		
	}

	public function display(){
		wp_editor( $this->getValue(), $this->getName(), $this->getAttrs() );
		return;
	}
	
}