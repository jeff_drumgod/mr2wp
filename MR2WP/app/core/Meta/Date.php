<?php
class MR2WP_Meta_Date extends MR2WP_Meta {

	public function __construct($name, $label, $attrs = null){

		$this->_template = "Input.php";
		$this->addHeadCallBack(array(&$this, 'adminHead'));

		if(isset($attrs["class"])){
			$attrs["class"] .= " datepicker ";
		} else {
			$attrs["class"] = "datepicker";
		}

		$attrs["type"] = "text";

		parent::__construct($name, $label, $attrs);

	}

	/**
	 * Call back acionado pela action admin_head
	 */
	public function adminHead(){

		wp_enqueue_script('jquery-ui');
		wp_enqueue_script('jquery-ui-datepicker');
		wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');

		?>
			<script type="text/javascript">
				jQuery(document).ready(function(){
					jQuery( "#<?php echo $this->getName(); ?>" ).datepicker({
						<?php
							foreach( $this->getOptions() as $option => $value){
								if( $i != count($this->_options) -1 )
									printf("%s: %s,", $option, $value);
								else
									printf("%s: %s", $option, $value);
							}
						?>
					});

					jQuery.datepicker.regional['pt-BR'] = {
		                closeText: 'Fechar',
		                prevText: '&#x3c;Anterior',
		                nextText: 'Pr&oacute;ximo&#x3e;',
		                currentText: 'Hoje',
		                monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
		                'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		                monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
		                'Jul','Ago','Set','Out','Nov','Dez'],
		                dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sabado'],
		                dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
		                dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
		                weekHeader: 'Sm',
		                dateFormat: 'dd/mm/yy',
		                firstDay: 0,
		                isRTL: false,
		                showMonthAfterYear: false,
		                yearSuffix: ''};
					jQuery.datepicker.setDefaults(jQuery.datepicker.regional['pt-BR']);

				});
			</script>
			<?php
		}

}