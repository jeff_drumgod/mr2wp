<?php
/**
 * MR2WP API - Configuração geral do framewok.
 * @author Marcel Rodrigo
 * @version 1.0
 * @date 2013-01-07
 * @category Wordpress
 * @link http://mr2wp.marcelrodrigo.com.br
 * @package MR2WP
 *
 */
class MR2WP_Core {

	private static $_instance;

	private function __construct(){
		if(is_admin()){
			$this->adminHead();	
		}
	}

	/**
	 * Call back acionado pela action admin_head
	 */
	public function adminHead(){
		wp_enqueue_style("mr2wp.css", MR2WP_URL . "/static/css/mr2wp.css");
		wp_enqueue_script("mr2wp.js", MR2WP_URL . "/static/js/mr2wp.js");
	}

	public static function getInstance(){
		if( !self::$_instance ){
			self::$_instance = new MR2WP_Core();
		}

		return self::$_instance;
	}

}