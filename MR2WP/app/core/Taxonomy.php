<?php
/**
 * Classe auxiliar para manipulação de Taxonomias
 *
 * @author Marcel Rodrigo
 * @version 1.0
 * @date 2013-01-07
 * @category Wordpress
 * @link http://mr2wp.marcelrodrigo.com.br
 * @package MR2WP
 *
 */
class MR2WP_Taxonomy {

	private $_name;
	private $_inColumn = false;
	private $_inFilter = false;
	private $_posttype;
	private $_args = array(
		'label' => '',
		'hierarchical' =>true,
		'show_ui' => true,
		'query_var' => true,
		'show_in_nav_menus' => true,
		'public' => true
	);

	/**
	* Cria uma nova instancia de MR2WP_Taxonomy.
	* @param string $name - Nome da taxonomia
	*/
	public function __construct( $name ){
		$this->_name = $name;
	}

	/**
	 * Define as propriedades da taxonomia.
	 * @param array $args
	 * @return MR2WP_Taxonomy
	 * @link http://codex.wordpress.org/Function_Reference/register_taxonomy#Arguments
	 */
	public function setArgs( array $args ){
		$this->_args = array_merge( $this->_args, $args );
		return $this;
	}

	/**
	 * Mostra os termos em uma coluna na listagem de posts.
	 * @return @MR2WP_Taxonomy
	 */
	public function showInColumn(){

		$this->_inColumn = true;

		return $this;
	}

	/**
	 * Adiciona um filtro de posts por termo na listagem de posts.
	 * @return MR2WP_Taxonomy
	 */
	public function showInFilter(){

		$this->_inFilter = true;

		return $this;

	}

	/**
	 * Define o post type para aplicar a taxonomia.
	 * @param MR2WP_PostType $posttype
	 * @return @MR2WP_Taxonomy
	 */
	public function setPostType( MR2WP_PostType $posttype ){

		$this->_posttype = $posttype;

		return $this;
	}

	/**
	* Salva a taxonomia
	* @return void
	*/
	public function save(){

		add_action('init', array(&$this, '__cb_tx_register'));

		if($this->_inFilter){

			add_action( 'restrict_manage_posts', array(&$this, '__cb_tx_insert_filter'));

			add_filter( 'parse_query', array(&$this, '__cb_tx_setup_filter'));

		}

		if($this->_inColumn){

			add_filter("manage_edit-{$this->_posttype->getName()}_columns", array( &$this, '__cb_tx_add_column'));

			add_action("manage_{$this->_posttype->getName()}_posts_custom_column", array(&$this, '__cb_tx_add_column_content'), 10, 3);
			do_action("manage_{$this->_posttype->getName()}_posts_custom_column", $this->_args['label'], $post_id, $this->_posttype->getName());
		}

	}

	/********************************************************************************************/
	/*** CALL BACKS
	/********************************************************************************************/

	public function __cb_tx_register(){

		register_taxonomy( $this->_name, $this->_posttype->getName(),  $this->_args);

	}

	public function __cb_tx_insert_filter(){

		global $typenow;

		if($typenow == $this->_posttype->getName()){

			$tax_obj = get_taxonomy($this->_name);
			$terms = get_terms($this->_name);

			echo "<select name='$this->_name' id='$this->_name' class='postform'>";
			echo "<option value=''>".$this->_args['label']."&nbsp;&nbsp;</option>";

			foreach ($terms as $term) {
			echo '<option value='. $term->slug, $_GET[$this->_name] == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>';
			}

			echo "</select>";
		}

	}

	public function __cb_tx_setup_filter($query){

		global $pagenow;

		if ( is_admin() && $pagenow=='edit.php' && isset($_GET['ADMIN_FILTER_FIELD_NAME']) && $_GET['ADMIN_FILTER_FIELD_NAME'] != '') {
			$query->query_vars['meta_key'] = $_GET['ADMIN_FILTER_FIELD_NAME'];
			if (isset($_GET['ADMIN_FILTER_FIELD_VALUE']) && $_GET['ADMIN_FILTER_FIELD_VALUE'] != '')
				$query->query_vars['meta_value'] = $_GET['ADMIN_FILTER_FIELD_VALUE'];
		}

	}

	public function __cb_tx_add_column($columns){
		$columns[$this->_name] = $this->_args['label'];

		return $columns;
	}

	public function __cb_tx_add_column_content($column_name, $post_id){
		global $custom_columns;

		if(!isset($custom_columns[$post_id][$column_name])){
			$taxonomy = $column_name;
			$terms = get_the_terms($post_id, $taxonomy);

			if ( $terms && !is_wp_error($terms) ) {
				$post_terms = array();
				foreach ( $terms as $term ){
					$post_terms[] = "<a href='edit.php?post_type={$this->_posttype->getName()}&{$taxonomy}={$term->slug}'> " . esc_html(sanitize_term_field('name', $term->name, $term->term_id, $taxonomy, 'edit')) . "</a>";
				}
				if($post_terms){
					echo join( ', ', $post_terms );
				}
			}

			$custom_columns[$post_id][$column_name] = true;
		}
	}

}