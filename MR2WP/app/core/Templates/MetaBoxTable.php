<input type="hidden" name="<?php echo $this->getID() ?>_nonce" value="" />
<table class="form-table mr2wp-metabox" id="<?php echo $this->getID(); ?>">
	<?php foreach($this->getMetas() as $meta): ?>
	<?php if($meta->getAttr("type") != "hidden"): ?>
	<tr>
		<th>
			<label for="<?php echo $meta->getName(); ?>"><?php echo $meta->getLabel(); ?></label>
		</th>
		<td>
			<?php echo $meta->display(); ?>
			<?php if($meta->getOption("description")): ?>
			<br/><span class="description"><?php echo $meta->getOption("description"); ?></span>
			<?php endif; ?>	
		</td>
	</tr>
	<?php else: ?>
	<tr>
		<td colspan="2"><?php echo $meta->display(); ?></td>
	</tr>
	<?php endif; ?>
	<?php endforeach; ?>
</table>