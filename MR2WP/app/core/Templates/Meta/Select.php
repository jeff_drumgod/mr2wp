<select 
	name="<?php echo $this->getName(); ?>" 
	id="<?php echo $this->getName(); ?>"
	<?php 
		foreach($this->getAttrs() as $attr => $value){
		
			printf("%s=\"%s\" ", $attr, $value);
		 	
		} 
	?>
>
<?php 
	if($this->getOptions()){
		foreach ($this->getOptions() as $option){
			?>
			<option 
				id="<?php echo $option->getName(); ?>"
				value="<?php echo $option->getName(); ?>"
				<?php echo $this->getValue() == $option->getName() ? "selected=\"selected\"" : "";  ?>
				<?php 
					if($option->getAttrs()){
						foreach($option->getAttrs() as $attr => $value){
						
							printf("%s=\"%s\" ", $attr, $value);
						 	
						} 
					}
				?>
			>
			<?php echo $option->getLabel(); ?>
			</option>
			<?php
		}
	}
?>
</select>