<?php if($this->getAttr("title")): ?>
<p><?php echo $this->getAttr("title"); ?></p>
<?php endif; ?>

<?php foreach ($this->getOptions() as $checkbox): ?>

	<input
		<?php
			if($checkbox->getAttrs()){
				foreach($checkbox->getAttrs() as $attr => $value){

					printf("%s=\"%s\" ", $attr, $value);

				}
			}
		?>
		type="checkbox"
		name="<?php echo $this->getName(); ?>[]"
		<?php
			if($this->getValue()){
				echo (in_array($checkbox->getName(), $this->getValue())) ? "checked=\"cheched\"" : "";
			}
		?>
		id="<?php echo $checkbox->getName(); ?>"
		value="<?php echo $checkbox->getName(); ?>"
	/>

	<label for="<?php echo $checkbox->getName(); ?>"><?php echo $checkbox->getLabel(); ?></label> <br />

<?php endforeach; ?>