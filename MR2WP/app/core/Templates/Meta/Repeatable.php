<a rel="<?php echo $this->getName(); ?>-repeatable" class="repeatable-add button" href="#">+</a>
<ul id="<?php echo $this->getName(); ?>-repeatable" class="custom_repeatable">
	<?php 
		if($this->getValue()) : 

			$values = $this->getValue();

			foreach( $values as $value ): 
	?>
		<!-- <span class="sort hndle">|||</span> -->
		<?php 
			$class = get_class($this->getControll());
			$controll = $this->getControll();
			
			$args = $controll->getAttrs();
			$args['value'] = $value;
			$newControll = new $class($this->getName()."[]", $controll->getLabel(), $args);
			
			printf('<li>%s<a class="repeatable-remove button" href="#">-</a></li>', $newControll->display());
					
		?>
		
		
	<?php 
			endforeach; 
		else:
	?>
	<li>
		<!-- <span class="sort hndle">|||</span> -->
		<?php echo $this->getControll()->display(); ?>
		
		<a class="repeatable-remove button" href="#">-</a>
	</li>
	<?php endif; ?>
</ul>