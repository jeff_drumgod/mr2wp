<div class="mr2wp-meta-gallery mr2wp-meta-video-gallery" id="<?php echo $this->getName(); ?>-container">

	<a href="#TB_inline?width=600&height=550&inlineId=<?php echo $this->getName(); ?>-modal" id="btn-<?php echo $this->getName(); ?>" class="meta_gallery_button button button-primary thickbox">Adicionar vídeo</a>
	<a href="javascript:void(0)" class="custom_clear_gallery_button button remover-todos">Remover todos</a>	

	<ul class="videos">
	<?php if($this->getValue()): foreach( $this->getValue() as $video ):  ?> 
		<li class="video">
			<?php printf('<img src="http://img.youtube.com/vi/%s/mqdefault.jpg" alt="%s" />', $video["id"], $video["title"]); ?>
			<a href="#" class="remove"></a>
		</li>
	<?php endforeach; endif; ?>
	</ul>

	<div class="clear"></div>

	<?php if($this->getValue()): foreach( $this->getValue() as $i => $video ): ?> 
	<input type="hidden" class="<?php echo $this->getName(); ?>_input" name="<?php printf("%s[%s][id]", $this->getName(), $i) ?>" value="<?php echo $video["id"]; ?>" />
	<input type="hidden" class="<?php echo $this->getName(); ?>_input" name="<?php printf("%s[%s][title]", $this->getName(), $i) ?>" value="<?php echo $video["title"]; ?>" />
	<?php endforeach; endif; ?>

	<!-- modal -->
	<div id="<?php echo $this->getName(); ?>-modal" class="modal-search">
		
		<div id="<?php echo $this->getName(); ?>-modal-container" class="mr2wp-meta-video-gallery-modal-container">
	    	<div class="search-bar">
	    		<input type="text" class="input" placeholder="URL do vídeo" />
	    		<a href="javascript:void(0)" class="button button-primary">Procurar</a>
	    	</div>

	    	<div class="video-info">
	    		<img src="" class="thumb">
	    		<div class="clear"></div>
	    		<label>Título</label>
	    		<input type="text" class="video-title" />
	    		<label>Descrição</label>
	    		<textarea class="video-description"></textarea>
	    	</div>

	    	<div class="clear"></div>

	    	<div class="actions">
	    		<a href="javascript:void(0)" class="button button-primary selecionar">Selecionar vídeo</a>
	    		<a href="javascript:void(0)" class="button cancelar">Cancelar</a>
	    	</div>
		</div>

	</div>
	<!-- modal [end] -->

</div>