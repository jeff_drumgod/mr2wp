<div class="googleMaps" id="<?php echo $this->getName() ?>-container">

	<input 
		type="text" 
		name="<?php echo $this->getName(); ?>-finder" 
		id="<?php echo $this->getName(); ?>-finder"
		class="finder"
	/>
	<button class="button button-primary">Procurar endereço</button>
	
	<input 
		type="text" 
		class="coords" 
		readonly="readonly" 
		name="<?php echo $this->getName() ?>" 
		id="<?php echo $this->getName(); ?>" 
		value="<?php echo $this->getValue(); ?>"
	/>
	
	<div id="<?php echo $this->getName(); ?>-map" class="map"></div>

</div>

