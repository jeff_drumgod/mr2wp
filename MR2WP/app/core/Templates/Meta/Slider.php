<div id="<?php echo $this->getName(); ?>-slider"></div>
<input
	type="text"
	name="<?php echo $this->getName(); ?>"
	id="<?php echo $this->getName(); ?>"
	readonly="readonly"
	<?php
		foreach($this->getAttrs() as $attr => $value){

			printf("%s=\"%s\" ", $attr, $value);

		}
	?>
/>