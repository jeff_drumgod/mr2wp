<fieldset id="<?php echo $this->_id; ?>" class="mr2wp-metabox mr2wp-user-metabox">
	<h3><?php echo $this->_title; ?></h3>
	<table class="form-table">

		<?php foreach ($this->_metas as $meta): ?>
		<tr>
			<th><label for="<?php echo $meta->getName(); ?>"><?php echo $meta->getLabel(); ?></label></th>
			<td><?php echo $meta->display(); ?></td>
		</tr>
		<?php endforeach; ?>
	</table>
</fieldset>