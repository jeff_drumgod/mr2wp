<input type="hidden" name="<?php echo $this->getID() ?>_nonce" value="" />

<div id="<?php echo $this->getID(); ?>">
	<?php foreach($this->getMetas() as $meta): ?>
		<p><?php echo $meta->getLabel(); ?></p>
		<?php echo $meta->display(); ?>	
		<br/><span class="description"><?php echo $meta->getOption("description"); ?></span>
	<?php endforeach; ?>
</div>