<?php
/**
 * A classe abstrata MR2WP_Meta facilita a manipulação de metas.
 * Esta é uma classe abstrata, por isso não pode ser instanciada, pois não representa nenhum elemento visual.
 * Para fazer uso de suas funcionalidade é preciso extendê-la.
 * @author Marcel Rodrigo
 * @version 1.3
 * @date 2013-01-07
 * @category Wordpress
 * @link http://mr2wp.marcelrodrigo.com.br
 * @package MR2WP
 */
abstract class MR2WP_Meta {

	private $_name;
	private $_label;
	private $_attrs;
	private $_options;

	private $_admin_head_cb = array();

	protected $_template;

	/**
	 * Cria uma instância de MR2WP_Meta
	 * @param string $name
	 * @param string $label
	 * @param array $attrs
	 * @param array $options
	 */
	public function __construct( $name, $label, $attrs = array(), $options = array() ){
		$this->_name = $name;
		$this->_label = $label;
		$this->_attrs = $attrs;
		$this->_options = $options;
	}

	/**
	 * Retorna o nome do post meta.
	 * @return string
	 */
	public function getName(){
		return $this->_name;
	}

	/**
	 * Retorna o label do post meta.
	 * @return string
	 */
	public function getLabel(){
		return $this->_label;
	}

	/**
	 * Retorna todos os atributos
	 * @return array
	 */
	public function getAttrs(){
		return $this->_attrs;
	}

	/**
	 * Retorna um atributo
	 * @param string $arg - nome do atributo
	 * @return mixed
	 */
	public function getAttr( $attr, $defautl = null ){
		return isset($this->_attrs[$attr]) ? $this->_attrs[$attr] : $defautl;
	}

	/**
	 * Retorna o atributo value
	 * @return mixed
	 */
	public function getValue(){
		return $this->getAttr("value");
	}

	/**
	 * Adiciona uma opção.
	 * @param string $option
	 * @param mixed $value
	 * @return MR2WP_Meta
	 */
	public function addOption( $option, $value ){
		$this->_options[$option] = $value;
		return $this;
	}

	/**
	 * Adiciona um atributo html.
	 * @param string $name
	 * @param string $value
	 * @return MR2WP_Meta
	 */
	public function addAttr( $name, $value ){
		$this->_attrs[$name] = $value;
		return $this;
	}

	/**
	 * Retorna uma opção
	 * @param string $option
	 * @return midex
	 */
	public function getOption( $option, $default = null ){
		return isset($this->_options[$option]) ? $this->_options[$option] : $default;
	}

	/**
	 * Retorna todas as opções
	 * @return mixed
	 */
	public function getOptions(){
		return $this->_options;
	}

	/**
	 * Adiciona uma função de call back na pilha.
	 * @param array:instance,method | function $callback
	 * @return MR2WP_Meta
	 */
	protected function addHeadCallBack( $callback ){
		$this->_admin_head_cb[] = $callback;
		return $this;
	}

	/**
	* Adiciona as funções da pilha de callbacks no hook admin_footer
	*/
	public function addActionAdminHead(){
		foreach($this->_admin_head_cb as $callback){
			add_action( "admin_footer", $callback);
		}
	}

	/**
	 * Bind the HTML for component
	 * @throws ErrorException - if template not exist
	 * @return string - html
	 */
	public function display(){

		$path = MR2WP_PATH . "/app/core/Templates/Meta/" . $this->_template;

		if(file_exists($path)){

			return $this->render();

		} else {

			throw new ErrorException("Template $this->_template not found.");

		}

	}

	/**
	 * -------------------------------------
	 * Render meta
	 * -------------------------------------
	 *
	 * @param null $viewData - any data to be used within the template.
	 * @return string -
	 *
	 */
	public function render( $viewData = null ) {

		$path = MR2WP_PATH . "/app/core/Templates/Meta/" . $this->_template;

		( $viewData ) ? extract( $viewData ) : null;
		ob_start();
		require ( $path );
		$template = ob_get_contents();
		ob_end_clean();
		return $template;
	}

}