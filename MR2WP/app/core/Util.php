<?php
/**
 * MR2WP_Util - Classe util do framework
 * @author Marcel Rodrigo
 * @version 1.0
 * @date 2013-20-12
 * @category Wordpress
 * @link http://mr2wp.marcelrodrigo.com.br
 * @package MR2WP
 *
 */
class MR2WP_Util {

	public static function get_youtube_video_id($url){
		$parsedUrl  = parse_url($url);  
        $embed      = $parsedUrl['query'];  
        parse_str($embed, $out);
        return isset($out['v']) ? $out['v'] : null;
	}

	public static function get_video_info($video_url){

		$video = null;

		if(strpos($video_url, "youtube") !== false){
			$video = self::get_youtube_video_info(self::get_youtube_video_id($video_url));
		}

		return $video;

	}

	private function get_video_mock(){
		$video = new stdClass();

		$video->host = "youtube";
		$video->id = 0;
		$video->embed_url = sprintf("http://localhost/embed/%s", $video_id);
		$video->title = "Quis nostrud exercitation ";
		$video->content = "ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui";
		
		$video->thumbs = array();
		$video->thumbs["hq"] = "http://localhost:9006/wp-content/uploads/2014/12/hqdefault.jpg";
		$video->thumbs["mq"] = "http://localhost:9006/wp-content/uploads/2014/12/hqdefault-300x225.jpg";
		$video->thumbs["default"] = "http://localhost:9006/wp-content/uploads/2014/12/hqdefault-150x150.jpg";

		return $video;
	}

	public static function get_youtube_video_info($video_id){
		$url = "http://gdata.youtube.com/feeds/api/videos/{$video_id}";
		$video = null;

		$xml = simplexml_load_file($url) or die($video = MR2WP_Util::get_video_mock());

		if($xml){
			$video = new stdClass();

			$video->host = "youtube";
			$video->id = $video_id;
			$video->embed_url = sprintf("http://www.youtube.com/embed/%s", $video_id);
			$video->title = (string)$xml->title;
			$video->content = (string)$xml->content;

			$video->thumbs = array();
			$video->thumbs["hq"] = sprintf("http://img.youtube.com/vi/%s/hqdefault.jpg", $video_id);
			$video->thumbs["mq"] = sprintf("http://img.youtube.com/vi/%s/mqdefault.jpg", $video_id);
			$video->thumbs["default"] = sprintf("http://img.youtube.com/vi/%s/default.jpg", $video_id);
		}

		return $video;
	}

	/**
	* Importa uma imagem atravéz da URL e define como imagem destacada de um post.
	* Função escrita por Remi Corson: http://www.wpexplorer.com/wordpress-featured-image-url/
	* @param string $image_url - URL da imagem
	* @return void
	*/
	public static function insertExteralAttachment ( $post_id, $image_url, $filename = null ){
		$upload_dir = wp_upload_dir(); // Set upload folder
		$image_data = file_get_contents($image_url); // Get image data
		
		if(!$filename)
			$filename = basename($image_url); // Create image file name
		else
			$filename = "{$filename}." . pathinfo($image_url, PATHINFO_EXTENSION);

		// Check folder permission and define file location
		if( wp_mkdir_p( $upload_dir['path'] ) ) {
		    $file = $upload_dir['path'] . '/' . $filename;
		} else {
		    $file = $upload_dir['basedir'] . '/' . $filename;
		}

		// Create the image  file on the server
		file_put_contents( $file, $image_data );

		// Check image file type
		$wp_filetype = wp_check_filetype( $filename, null );

		// Set attachment data
		$attachment = array(
		    'post_mime_type' => $wp_filetype['type'],
		    'post_title'     => sanitize_file_name( $filename ),
		    'post_content'   => '',
		    'post_status'    => 'inherit'
		);

		// Create the attachment
		$attach_id = wp_insert_attachment( $attachment, $file, $post_id );

		// Include image.php
		require_once(ABSPATH . 'wp-admin/includes/image.php');

		// Define attachment metadata
		$attach_data = wp_generate_attachment_metadata( $attach_id, $file );

		// Assign metadata to attachment
		wp_update_attachment_metadata( $attach_id, $attach_data );

		// And finally assign featured image to post
		set_post_thumbnail( $post_id, $attach_id );
	}

}