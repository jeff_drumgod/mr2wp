<?php

$json = new stdClass();
$json->errors = array();

if( isset($_POST["image_url"]) ){

	$filename = isset($_POST["filename"]) ? $_POST["filename"] : md5(date("ymdHis"));

	$image_url = esc_url($_POST["image_url"]);
	$contents = file_get_contents($image_url);

	if($contents){

		$ext = pathinfo($image_url, PATHINFO_EXTENSION);

		file_put_contents("{$filename}.{$ext}", $contents);

	}

} else {
	
	$json->result = false;
	$json->errors[] = "Invalid request.";
	
}

echo json_encode($json);