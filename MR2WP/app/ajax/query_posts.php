<?php

$json = new stdClass();
$json->errors = array();

if( !empty($_GET)){

	unset($_GET["action"]);

	$query = new WP_Query( $_GET );
	
	$json->result = "success";
	$json->post_count = $query->post_count;
	$json->posts = $query->posts;
	
	
} else {
	
	$json->result = "error";
	$json->errors[] = "Invalid argument.";
	
}

echo json_encode($json);