# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "css"
sass_dir = "css/scss"
images_dir = "img"
javascripts_dir = "js"

# The environment mode. Defaults to :production, can also be :development
# compass compile -e production --force
environment = :development

# You can select your preferred output style here (can be overridden via the command line):
# output_style = :expanded or :nested or :compact or :compressed
# output_style = :expanded
output_style = (environment == :production) ? :compressed : :expanded

# To enable relative paths to assets via compass helper functions. Uncomment:
#relative_assets = (environment != :production)
#http_path = (environment == :production) ? "/arquivos/" : nil
#http_generated_images_path = (environment == :production) ? "/arquivos/" : nil

relative_assets = false
http_path = "/static/"
http_generated_images_path = "/static/img/"

# To disable debugging comments that display the original location of your selectors. Uncomment:
# line_comments = false


# If you prefer the indented syntax, you might want to regenerate this
# project again passing --syntax sass, or you can uncomment this:
# preferred_syntax = :sass
# and then run:
# sass-convert -R --from scss --to sass css/scss scss && rm -rf sass && mv scss sass
preferred_syntax = :scss

sass_options = {:custom => { :venv => {:in_use => environment} } }
#Script para declaração dinâmica de caminho de imagens, para ser usado nas declarações de background.
module Sass::Script::Functions
  def imagesPath
    path = (options[:custom][:venv][:in_use] == :production) ? "/img/" : "/img/"
    Sass::Script::String.new(path)
  end
  def imagesPathDisk
    path2 =  "../img/"
    Sass::Script::String.new(path2)
  end
end