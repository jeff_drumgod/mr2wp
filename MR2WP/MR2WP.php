<?php

/**
* MR2WP Framework - v1.1.2 - 2013-08-26
* URL: http://mr2wp.marcelrodrigo.com.br
* Author: Marcel Rodrigo
* Repository: https://bitbucket.org/marcelrodrigo/mr2wp
**/

define("MR2WP_PATH", dirname(__FILE__));
define("MR2WP_URL", get_bloginfo( "template_url" ) . "/MR2WP");

$MR2WP = MR2WP_Core::getInstance();

// autoload classes
function __autoload( $name ){

	preg_match("~MR2WP.*~", $name, $matches);

	if( isset($matches[0]) ){

		$path = str_replace("_", "/", $name);
		$path = str_replace("MR2WP", "", $path);
		$path = MR2WP_PATH . "/app/core" . $path . ".php";

		if( file_exists($path) ){
			require($path);
		}
	}
}