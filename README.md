# MR2WP Wordpress Framework 1.1 #
O MR2WP é um framework que facilita no desenvolvimento de Temas e Plugins para Wordpress.

É altamente extensível, possibilitando o desenvolvedor exterder funcionalidades para um melhor aproveitamento do CMS.

**Classes inclusas**

-  MR2WP_Admin_Page
-  MR2WP_Rewrite
-  MR2WP_PostType
-  MR2WP_Taxonomy
-  MR2WP_MetaBox
-  MR2WP_Meta

Descubra mais na [documentação oficial.](http://mr2wp.marcelrodrigo.com.br)

## Instalação ##
Copie a pasta MR2WP para dentro do seu tema, para que não haja nenhum encômodo, cole na raiz do tema.

Caso não deixe na raiz do tema, será necessário fazer a seguinte alteração no arquivo *MR2WP.php* na linha 11:

    define("MR2WP_URL", get_bloginfo( "template_url" ) . "/MR2WP");
para:

    define("MR2WP_URL", get_bloginfo( "template_url" ) . "/NOVO_DIRETORIO/MR2WP");

Essa alteração é necessária para configurar corretamente a constante da URL do framework.

Feito isso inclua no seu arquivo *functions.php* o arquivo MR2WP.php:

    include dirname(__FILE__) . "/MR2WP/MR2WP.php";

E pronto! O MR2WP framework já está instalado.

## Contribuindo ##
Para contribuir no desenvolvimento é necessário ter o [npm](http://npmjs.org) rodando no [node.js](http://nodejs.org), pois o MR2WP conta com o [grunt.js](http://gruntjs.com) em e algumas rotinas:

- Manter a versão atualizada no diretório de exemplos.
- Rodar o [Compass](http://compass-style.org).

É extremamente necessário executar as tasks do grunt para iniciar o desenvolvimento, para que não se perca nada.

Basta executar a task default do grunt e pronto! *Go to work!*

    grunt


