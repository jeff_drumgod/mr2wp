module.exports = function(grunt) {

  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),
    dirs: {
      code: 'MR2WP',
      theme: 'exemplo/wordpress/wp-content/themes/mr2wp',
      static: 'MR2WP/static'
    },

    compass: {
      compile: {
        options: {
          //config: '<%= dirs.static %>/config.rb'
          sassDir: '<%= dirs.static %>/css/scss',
          cssDir: '<%= dirs.static %>/css',
          environment: 'development'
        },
      }
    },

    copy: {
      main: {
        files: [
          {expand: true, cwd: '<%= dirs.code %>', src: ['**', '!**/scss/**', '!**/.sass-cache/**', '!**/config.rb'], dest: '<%= dirs.theme %>/MR2WP/'}
        ]
      }
    },

    watch: {
      options:{
        livereload: true,
        nospawn: true
      },
      compass: {
        files: ['<%= dirs.static %>/**/*.scss'],
        tasks: ['compass']
      },
      copy: {
        files: ['<%= dirs.code %>/**'],
        tasks: ['copy']
      }
    }

  });

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-copy');

  // Default task.
  grunt.registerTask('default', ['watch']);

};
